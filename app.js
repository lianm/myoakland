// Load Dependencies
// -----------------
engine = require('./engine/engine');
var _ = require('lodash');
var url = require("url");
var Q = require('q');
var async = require('async');


// Configure the engine
// --------------------
engine.config = {
  port: process.env.PORT || 8185,
  dbUri: process.env.DB_URI,
  apiPrefix: "/api"
};

pageRoutes = require('./engine/pageroutes.js')(engine.express.app);
sitemap = require('./engine/sitemap.js')(engine.express.app);


/**
 * Initialise
 * ----------
 * Initialise the app and mount the models and routes
 */
engine.init();


