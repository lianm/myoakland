/*
This is a simple example of defining a model, simply define the schema and enter any middleware
in the module.exports call
 */

var mongoose = require('mongoose');
var middleware = require('../middleware');

var schema = {
  name: String,
  description: String,
  housePrice: Number,
  houseSize: Number, // m2
  bedrooms: Number,
  bathrooms: Number,
  loungerooms: Number,
  cars: Number,
  houseLevels: Number,
  duplex: Boolean,
  builder: {type: mongoose.Schema.Types.ObjectId, ref: 'Builders'},
  associatedFiles: [{
    fileId: {type: mongoose.Schema.Types.ObjectId, ref: 'Files'},
    fileType: {type: mongoose.Schema.Types.ObjectId, ref: 'FileTypes'},
    mimetype: String,
    storeName: String
  }]
};

module.exports = {
  middleware: {
    find: [],
    create: [],
    update: [],
    remove: [],
    findById: [],
    search: []
  },
  model: mongoose.model('Homes', schema)
};
