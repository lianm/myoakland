/*
This is a simple example of defining a model, simply define the schema and enter any middleware
in the module.exports call
 */

var mongoose = require('mongoose');
var middleware = require('../middleware');
var diffHistory = require('mongoose-diff-history/diffHistory');

var schema = new mongoose.Schema({
  projectName: String,
  projectCode: String,
  
  createdAt: { type: Date, default: Date.now },
  createdBy: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  updatedAt: { type: Date, default: Date.now },
  updatedBy: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
});

schema.plugin(diffHistory.plugin);

module.exports = {
  middleware: {
    find: [],
    create: [],
    update: [],
    remove: [],
    findById: [],
    search: []
  },
  model: mongoose.model('Projects', schema)
};
