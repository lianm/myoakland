/*
This is a simple example of defining a model, simply define the schema and enter any middleware
in the module.exports call
 */

var mongoose = require('mongoose');
var middleware = require('../middleware');

var schema = {
  originalName: String,
  storeName: String,
  newName: String,
  mimetype: String,
  metaData: {type: mongoose.Schema.Types.Mixed},
  fileType: {type: mongoose.Schema.Types.ObjectId, ref: 'FileTypes'},
  
  simpleName: String,
  Description: String,
  
  clientAccess: Boolean,
  supplierAccess: Boolean,
  archive: Boolean,
  
  // GENERAL PAGES
  user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  pages: {type: mongoose.Schema.Types.ObjectId, ref: 'Pages'},
  contentBlocks: {type: mongoose.Schema.Types.ObjectId, ref: 'ContentBlocks'},
  
  // ESCAPE HOME FILES MANAGEMENT
  business: {type: mongoose.Schema.Types.ObjectId, ref: 'Business'},
  project: {type: mongoose.Schema.Types.ObjectId, ref: 'Projects'},
  item: {type: mongoose.Schema.Types.ObjectId, ref: 'Items'},
  colour: {type: mongoose.Schema.Types.ObjectId, ref: 'Colours'},
  
  // CLIENT WEBSITE FILE MANAGEMENT
  land: {type: mongoose.Schema.Types.ObjectId, ref: 'Land'},
  package: {type: mongoose.Schema.Types.ObjectId, ref: 'Packages'}
  
};

module.exports = {
  middleware: {
    find: [],
    create: [],
    update: [],
    remove: [],
    findById: [],
    search: []
  },
  model: mongoose.model('Files', schema)
};
