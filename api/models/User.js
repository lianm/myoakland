/*
This is a simple example of defining a model, simply define the schema and enter any middleware
in the module.exports call
 */

var mongoose = require('mongoose');
var middleware = require('../middleware');

var schema = {
  firstname: String,
  lastname: String,
  email: {type: String, unique: true},

  photos: [{
    originalName: String,
    storeName: String
  }],

  latestBraintreeInfo: {type: mongoose.Schema.Types.Mixed},

  password: {type: String, select: false},
  created_at: String,
  updated_at: String,
  role_admin: Boolean
};

module.exports = {
  middleware: {
    find: [],
    create: [],
    update: [],
    remove: [],
    findById: [],
    search: []
  },
  model: mongoose.model('User', schema)
};
