/*
This is a simple example of defining a model, simply define the schema and enter any middleware
in the module.exports call
 */

var mongoose = require('mongoose');
var middleware = require('../middleware');

var schema = {
  lotId: String, // stage_lotNumber
  lotNumber: Number,
  stage: String,
  landPrice: Number,
  landSize: Number, // m2
  frontage: Number, // m
  depth: Number,
  corner: Boolean,
  hold: Boolean,
  sold: Boolean,
  associatedFiles: [{
    fileId: {type: mongoose.Schema.Types.ObjectId, ref: 'Files'},
    fileType: {type: mongoose.Schema.Types.ObjectId, ref: 'FileTypes'},
    mimetype: String,
    storeName: String
  }]
};

module.exports = {
  middleware: {
    find: [],
    create: [],
    update: [],
    remove: [],
    findById: [],
    search: []
  },
  model: mongoose.model('Land', schema)
};
