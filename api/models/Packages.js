/*
This is a simple example of defining a model, simply define the schema and enter any middleware
in the module.exports call
 */

var mongoose = require('mongoose');
var middleware = require('../middleware');

var schema = {
  packageId: String,
  land: {type: mongoose.Schema.Types.ObjectId, ref: 'Land'},
  home: {type: mongoose.Schema.Types.ObjectId, ref: 'Homes'},
  packagePrice: Number,
  
  // from land
  stage: String,
  lotNumber: Number,
  landPrice: Number, // get from land on save
  landSize: Number, // m2
  
  // from homes
  builder: {type: mongoose.Schema.Types.ObjectId, ref: 'Builders'},
  builderName: String,
  houseName: String,
  buildPrice: Number, // get from package price less land price on save
  houseSize: Number, // m2
  bedrooms: Number,
  bathrooms: Number,
  loungerooms: Number,
  cars: Number,
  houseLevels: Number,
  
  packageBuildPrice: Number,
  fixedPackageBuildPrice: Boolean,
  
  
  associatedFiles: [{
    fileId: {type: mongoose.Schema.Types.ObjectId, ref: 'Files'},
    fileType: {type: mongoose.Schema.Types.ObjectId, ref: 'FileTypes'},
    mimetype: String,
    storeName: String
  }]
};

module.exports = {
  middleware: {
    find: [],
    create: [],
    update: [],
    remove: [],
    findById: [],
    search: []
  },
  model: mongoose.model('Packages', schema)
};
