/*
This is a simple example of defining a model, simply define the schema and enter any middleware
in the module.exports call
 */

var mongoose = require('mongoose');
var middleware = require('../middleware');

var schema = {
  name: String,
  geo: {
    lat: Number,
    lng: Number
  },
  description: String,
  category: {type: mongoose.Schema.Types.ObjectId, ref: 'LocationCategory'}
};

module.exports = {
  middleware: {
    find: [],
    create: [],
    update: [],
    remove: [],
    findById: [],
    search: []
  },
  model: mongoose.model('LocationPoints', schema)
};
