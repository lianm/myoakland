/*
This is a simple example of defining a model, simply define the schema and enter any middleware
in the module.exports call
 */

var mongoose = require('mongoose');
var middleware = require('../middleware');

var schema = {
  intro: {
    title: String,
    content: String
  },
  why: {
    title: String,
    content: String,
    contentAreas: [{
      title: String,
      content: String,
      link: String,
      buttonText: String,
      backgroundColor: String,
      textColor: String
    }]
  },
  associateLinks: {
    title: String,
    logoLinks: [{
      originalName: String,
      storeName: String,
      originalWidth: Number,
      originalHeight: Number,
      name: String,
      link: String
    }]
  },
  sliderImages: [{
    originalName: String,
    storeName: String,
    originalWidth: Number,
    originalHeight: Number
  }]
};

module.exports = {
  middleware: {
    find: [],
    create: [],
    update: [],
    remove: [],
    findById: [],
    search: []
  },
  model: mongoose.model('Homepage', schema)
};
