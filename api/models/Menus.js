/*
This is a simple example of defining a model, simply define the schema and enter any middleware
in the module.exports call
 */

var mongoose = require('mongoose');
var middleware = require('../middleware');

var schema = {
  menuId: {type: String, unique: true},
  name: String,
  menuCssClass: String,
  parentMenus: [{
    name: String,
    link: String,
    subMenus: [{
      name: String,
      link: String
    }],
    subMenuLink: {type: mongoose.Schema.Types.ObjectId, ref: 'Menus'},
    useSubMenuLink: Boolean,
    subMenuCssClass: String
  }]
};

module.exports = {
  middleware: {
    find: [],
    create: [],
    update: [],
    remove: [],
    findById: [],
    search: []
  },
  model: mongoose.model('Menus', schema)
};
