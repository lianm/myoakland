/*
This is a simple example of defining a model, simply define the schema and enter any middleware
in the module.exports call
 */

var mongoose = require('mongoose');
var middleware = require('../middleware');

var schema = {
  name: String,
  email: String,
  phone: String,
  message: String,
  additionalFormData: {type: mongoose.Schema.Types.Mixed},
  
  
  reason: String, // Reason for enquiry (1st home buyers grant, package, etc.)
  referral: String, // How did you hear about us?
  created: {type : Date, default : Date.now},
  allFormData: {type: mongoose.Schema.Types.Mixed},
  formType: String, // what the form is for
  formPlacement: String // where it has been placed on the page
  
};

module.exports = {
  middleware: {
    find: [],
    create: [],
    update: [],
    remove: [],
    findById: [],
    search: []
  },
  model: mongoose.model('Emails', schema)
};
