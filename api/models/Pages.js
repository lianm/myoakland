/*
This is a simple example of defining a model, simply define the schema and enter any middleware
in the module.exports call
 */

var mongoose = require('mongoose');
var middleware = require('../middleware');

var schema = {
  name: String,
  pageContent: String,
  sliderImages: [{
    originalName: String,
    storeName: String,
    originalWidth: Number,
    originalHeight: Number
  }],
  cms: {
    pageUrl: String,
    pageTemplate: String,
    showInSitemap: Boolean,
    includeModels: String,
    published: Boolean,
    created_at: Date,
    created_by: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    updated_at: { type: Date, default: Date.now },
    updated_by: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
  },
  contentBlocks: [{
    blockTitle: String,
    blockType: {type: mongoose.Schema.Types.ObjectId, ref: 'ContentBlockTypes'},
    layout: String
  }],
  headerSlider: {type: mongoose.Schema.Types.ObjectId, ref: 'ContentBlockTypes'},
  pageType: {type: mongoose.Schema.Types.ObjectId, ref: 'PageTypes'},
  hideHero: Boolean,
  coverHero: Boolean,
  associatedFiles: [{
    fileId: {type: mongoose.Schema.Types.ObjectId, ref: 'Files'},
    fileType: {type: mongoose.Schema.Types.ObjectId, ref: 'FileTypes'},
    mimetype: String,
    storeName: String
  }]
};

module.exports = {
  middleware: {
    find: [],
    create: [],
    update: [],
    remove: [],
    findById: [],
    search: []
  },
  model: mongoose.model('Pages', schema)
};
