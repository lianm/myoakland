var Emails = require('../models/Emails').model;
var middleware = require('../middleware');
var _ = require('lodash');



// var sendgrid_api_key = "4m99BWlvQHiWA8pju3XXRA";
var sendgrid_api_key = "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";
var sendgrid  = require('sendgrid')(sendgrid_api_key);


var emailDetails = {
  fromAddress: 'somerfield@altitudepm.com.au',
  fromName: 'Somerfield Website Enquiry',
  subject: 'Somerfield Estate - Website Enquiry'
}

/*
 var mandrill = require('mandrill-api/mandrill');
 var m = new mandrill.Mandrill('m5slshAtJFkKrDEGy_HPCQ');
 m.users.info(function(info) {
 console.log('Reputation: ' + info.reputation + ', Hourly Quota: ' + info.hourly_quota);
 });
 */


function findall(req, res) {
  Emails.find({}, function (err, docs) {
    res.json(docs);
  })
}



function findPage(req, res) {
  Emails.find(req.body, function (err, docs) {
    res.json(docs);
  })
}


function remove(req, res) {
  var myID = req.body._id;
  console.log("myID: " + myID);
  
  Emails.findOneAndRemove({_id: myID}, function (err, doc) {
    if (err) {
      res.json(err);
    } else {
      if (!doc) {
        res.json(404, {message: "Delete failed, record not found"});
      } else {
        console.log("user deleted");
        Emails.find({}, function (err, docs) {
          res.json(docs);
        })
      }
    }
  })
}



var mandrill = require('mandrill-api/mandrill');
var mandrill_client = new mandrill.Mandrill('zZgLDyP-JF_5CUNtIx-m5Q');
function sendBrochureMandrill(req, res) {
  var sendParams = {
    "template_name": "mandrill-test",
    "template_content": [],
    "message": {
      // "html": "<p>Example HTML content</p>",
      // "text": "Example text content",
      // "subject": "example subject",
      "from_email": "james@altitudepm.com.au",
      "from_name": "Mandrill Test Email",
      "to": [{
        "email": "james@masini.com.au",
        "name": "James Masini",
        "type": "to"
      },{
        "email": "marketing@altitudepm.com.au",
        "name": "Marketing",
        "type": "to"
      }],
      "auto_text": true,
      "merge": true,
      "merge_language": "handlebars",
      "global_merge_vars": [
        {
          "name": "name",
          "content": "Example Name"
        },
        {
          "name": "estateName",
          "content": "Somerfield Estate - Holmview (Test)"
        },
        {
          "name": "estateFiles",
          "content": [
            {
              "name": "test",
              "website": "http://www.masini.com.au"
            },
            {
              "name": "test2",
              "website": "http://www.masini2.com.au"
            }
          ]
        }
      ],
      "tags": [
        "downloadBrochure"
      ]
    }
  };
  mandrill_client.messages.sendTemplate(sendParams, function(result) {
    res.json(result);
  }, function(e) {
    res.json({error: "Mandrill Error", error_response: e})
  });
}



var fileTest = '<a href="https://drive.google.com/open?id=1akoEzbYwu8rcliDk0M39mNlVCy6wN7Ej">Estate Brochure</a>';

function sendBrochure(req, res) {
  var sgMail = require('@sendgrid/mail');
  sgMail.setApiKey(sendgrid_api_key);
  sgMail.setSubstitutionWrappers('{{', '}}'); // Configure the substitution tag wrappers globally
  var msg = {
    from: 'james@altitudepm.com.au',
    to: 'james@masini.com.au',
    // subject: 'Sending with SendGrid is Fun',
    // text: 'and easy to do anywhere, even with Node.js',
    // html: '<strong>and easy to do anywhere, even with Node.js</strong>',
    templateId: '46c5fd92-91f1-4199-a000-c82be9be444c',
    substitutionWrappers: ['{{', '}}'],
    substitutions: {
      "name": "James Test",
      "estateName": "Somerfield Estate, Holmview",
      "estateFiles": fileTest
    }
  };
  sgMail
    .send(msg)
    .then(function(result) {
      res.json(result);
    })
    .catch(function(error) {
      res.json({error: true, errorMessage: error});
    });
}



var helper = require('sendgrid').mail;
function send(req, res) {
  var submitedValues = JSON.parse(JSON.stringify(req.body));
  var thisuser = {
    name: submitedValues.name,
    email: submitedValues.email,
    phone: submitedValues.phone,
    message: submitedValues.message,
    referral: submitedValues.referral,
    reason: submitedValues.reason,
    allFormData: submitedValues,
    formType: submitedValues.formType,
    formPlacement: submitedValues.formPlacement
  }
  console.log("add this: ", thisuser);
  
  Emails.create(thisuser, function (err, doc) {
    if (err) {
      console.log("error creating user", err);
      res.json(err);
    } else {
      req.session.registration = doc;
      req.session.save();
      
      var newEmail = new helper.Mail();
      var fromEmail = new helper.Email(emailDetails.fromAddress, emailDetails.fromName);
      newEmail.setFrom(fromEmail);
      
      var Personalization = new helper.Personalization();
      
      var toEmail = new helper.Email(emailDetails.fromAddress, emailDetails.fromName);
      Personalization.addTo(toEmail);
  
      var toEmail = new helper.Email('james@altitudepm.com.au', 'James Masini');
      Personalization.addCc(toEmail);
  
      Personalization.setSubject(emailDetails.subject);
      newEmail.addPersonalization(Personalization);
  
      var emailMessage = '';
      console.log("thisuser: ", thisuser);
      var docKeys = Object.keys(thisuser);
      console.log("docKeys: ", docKeys);
      for (var i = 0; i < docKeys.length; i++) {
        emailMessage += '<div>' + docKeys[i] + ': ' + thisuser[docKeys[i]] + '</div>';

        if (i === docKeys.length -1) {
          console.log("emailMessage: ", emailMessage);
          
          var content = new helper.Content("text/html", emailMessage);
          newEmail.addContent(content);
  
          var request = sendgrid.emptyRequest({
            method: 'POST',
            path: '/v3/mail/send',
            body: newEmail.toJSON()
          });
  
          sendgrid.API(request, function(err, result) {
            if (err) {
              console.log("Message Sending Error", err);
              res.json({error: true, message: err});
            } else {
              console.log("Message Sent: ", result);
              var respondThis = {
                response: result,
                doc: doc
              };
              res.json(respondThis);
            }
          });
        }
      }
      
      
      
    }
  });
}


function create(req, res) {
  var thisuser = req.body;
  console.log("add this: ", thisuser);
  
  if (thisuser._id) {
    Emails.findOneAndUpdate({_id: thisuser._id}, thisuser, function (err, doc) {
      if (err) {
        console.log("error updating user", err)
        res.json(err);
      } else {
        Emails.find({}, function (err, doc) {
          res.json(doc);
        })
      }
    });
  } else {
    Emails.create(thisuser, function (err, doc) {
      if (err) {
        console.log("error creating user", err)
        res.json(err);
      } else {
        Emails.find({}, function (err, doc) {
          res.json(doc);
        })
      }
    });
  }
}

module.exports = {
  routes: [
    {
      path: "findall",
      method: "get",
      fn: findall,
      middleware: []
    },
    {
      path: "create",
      method: "post",
      fn: create,
      middleware: []
    },
    {
      path: "send",
      method: "post",
      fn: send,
      middleware: []
    },
    {
      path: "findPage",
      method: "post",
      fn: findPage,
      middleware: []
    },
    {
      path: "remove",
      method: "post",
      fn: remove,
      middleware: []
    },
    {
      path: "sendBrochure",
      method: "post",
      fn: sendBrochure,
      middleware: []
    },
    {
      path: "sendBrochureMandrill",
      method: "post",
      fn: sendBrochureMandrill,
      middleware: []
    }
  ]
};
