var Files = require('../models/Files').model;
var _ = require('lodash');
var Q = require('q');
var fs = require('fs');
var path = require('path');
var async = require('async');


/**
 * GOOGLE CLOUD STORAGE PARAMETERS
 **/

var gcloud = require('google-cloud');
var storage = gcloud.storage;
var keyPath = path.normalize(__dirname + '/../../engine/lib/GoogleCloud-AltitudePM.json');
var gcs = storage({
  projectId: 'altitudepm-155004',
  keyFilename: keyPath
});
var bucket = gcs.bucket('clientstorage.altitudepm.com.au');
var defaultFolder = 'somerfieldestate/';

/**
 * END GOOGLE CLOUD STORAGE PARAMETERS
 **/




/**
 * MULTER (FILE HANDLING) PARAMETERS - INC. FILE NAME SETUP
 **/
var multer  = require('multer');


// SAVE USING TEMP DIRECTORY ON THE SERVER
var tempStorageLocation = path.join(__dirname, '..', 'tempStorage');
var multerStorage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, tempStorageLocation)
  },
  filename: function (req, file, cb) {
    var oldName = file.originalname;
    var oldNameFileType = "." + oldName.substr(oldName.lastIndexOf('.')+1);
    var oldNameWithoutType = oldName.substr(0, oldName.lastIndexOf('.'));
    var newName = oldNameWithoutType.replace(/\s+/g,"_") + '_' + Date.now() + oldNameFileType;
    var newNameUrlEncoded = encodeURIComponent(newName);
    cb(null, newNameUrlEncoded)
  }
});
var tempStorage = multer({
  storage: multerStorage,
  fileFilter: function (req, file, cb) {
    /*
     if (file.mimetype !== 'image/png') {
     req.fileValidationError = 'goes wrong on the mimetype';
     return cb(null, false, new Error('goes wrong on the mimetype'));
     }
     */
    cb(null, true);
  }
}).single('file');




// NEW TEST
var multerMemory = multer({
  storage: multer.MemoryStorage,
  limits: {
    fileSize: 5 * 1024 * 1024 // no larger than 5mb
  }
}).single('file');

function uploadTest(req, res) {
  multerMemory(req, res, function (err) {
    if (err) {
      console.error(err);
      res.json({error: err});
    } else if (req.file) {
      console.log("*** file: ", req.file);
      saveFileToGoogleStorage(req.file, req.body.directory)
        .then(function(googleStorageResult) {
          var saveThis = {};
          if (req.body.currentAssociations) {
            saveThis = req.body.currentAssociations;
          }
          saveThis.originalName = req.file.originalname;
          saveThis.mimetype = req.file.mimetype;
          saveThis.newName = googleStorageResult.newFileName;
          saveThis.storeName = googleStorageResult.name;
          saveThis.metaData = googleStorageResult.metadata;
          if (req.body.uploadType && req.body.uploadTypeId) {
            saveThis[req.body.uploadType] = req.body.uploadTypeId;
          }
          Files.create(saveThis, function (err, doc) {
            if (err) {
              console.log("error creating file", err);
              res.json(err);
            } else {
              res.json(doc);
            }
          });
        })
    } else {
      res.json({error: 'unable to read file'});
    }
  })
  
}


/**
 * END MULTER (FILE HANDLING) PARAMETERS - INC. FILE NAME SETUP
 **/






function saveFileToGoogleStorage(file, directory) {
  var deferred = Q.defer();
  
  var uploadOptions = {
    destination: defaultFolder + "unknown/" + file.filename
  };
  if (directory) {
    uploadOptions.destination = defaultFolder + directory + file.filename;
  }
  
  bucket.upload(file.path, uploadOptions, function(err, result) {
    if (!err) {
      var returnThis = result;
      returnThis.newFileName = file.filename;
      fs.unlinkSync(tempStorageLocation + '/' + file.filename);
      deferred.resolve(returnThis);
    } else {
      fs.unlinkSync(tempStorageLocation + '/' + file.filename);
      console.log("file error: ", err);
      deferred.resolve(err);
    }
  });
  
  return deferred.promise;
}






function uploadFiles(req, res) {
  tempStorage(req, res, function (err) {
    if (err) {
      console.error(err);
      res.json({error: err});
    } else if (req.file) {
      console.log("*** file: ", req.file);
      saveFileToGoogleStorage(req.file, req.body.directory)
        .then(function(googleStorageResult) {
          var saveThis = {};
          if (req.body.currentAssociations) {
            saveThis = req.body.currentAssociations;
          }
          saveThis.originalName = req.file.originalname;
          saveThis.mimetype = req.file.mimetype;
          saveThis.newName = googleStorageResult.newFileName;
          saveThis.storeName = googleStorageResult.name;
          saveThis.metaData = googleStorageResult.metadata;
          if (req.body.uploadType && req.body.uploadTypeId) {
            saveThis[req.body.uploadType] = req.body.uploadTypeId;
          }
          Files.create(saveThis, function (err, doc) {
            if (err) {
              console.log("error creating file", err);
              res.json(err);
            } else {
              res.json(doc);
            }
          });
        })
    } else {
      res.json({error: 'unable to read file'});
    }
  })
}

function findallPost(req, res) {
  Files.find(req.body)
    .exec(function (err, docs) {
      if (err) {
        res.status(500).json(err);
      } else {
        if (docs) {
          res.json(docs);
        } else {
          res.status(400).json({message: "unknown"})
        }
      }
    })
}

function findallPopulate(req, res) {
  Files.find(req.body)
    .populate(['project', 'user', 'business', 'item', 'colour'])
    .exec(function (err, docs) {
      if (err) {
        res.status(500).json(err);
      } else {
        if (docs) {
          res.json(docs);
        } else {
          res.status(400).json({message: "unknown"})
        }
      }
    })
}


function findLandFiles(req, res) {
  Files.find({land: {$ne: null}})
    .exec(function (err, docs) {
      if (err) {
        res.status(500).json(err);
      } else {
        if (docs) {
          var collatedFiles = {};
          for (var i = 0; i < docs.length; i++) {
            if (!collatedFiles[docs[i].land]) {
              collatedFiles[docs[i].land] = {};
              collatedFiles[docs[i].land].files = [];
            }
            collatedFiles[docs[i].land].files.push(docs[i]);
            if (docs[i].fileType) {
              if (docs[i].fileType.toString() === "59c0b28744c464c9c912a9ba") {
                collatedFiles[docs[i].land].disclosureImage = docs[i];
              }
              if (docs[i].fileType.toString() === "59c050a1074c13c12070aa71") {
                collatedFiles[docs[i].land].disclosurePdf = docs[i];
              }
            }
          }
          res.json(collatedFiles);
        } else {
          res.status(400).json({message: "unknown"})
        }
      }
    })
}



fileGroups = ['user', 'pages', 'contentBlocks', 'business', 'project', 'item', 'colour', 'land', 'package'];
var fileTypesModel = require('../models/FileTypes').model;

function getFileTypes() {
  var deferred = Q.defer();
  
  fileTypesModel.find({}, function (err, docs) {
    var returnResults = {};
    for (var i = 0; i < docs.length; i++) {
      returnResults[docs[i]._id] = docs[i];
    
      if (i === docs.length -1) {
        deferred.resolve(returnResults);
      }
    }
  });
  
  return deferred.promise;
}

function getAllSearchedFiles(searchThis) {
  var deferred = Q.defer();
  
  Files.find(searchThis)
    .exec(function (err, docs) {
      if (err) {
        deferred.reject(new Error(err));
      } else {
        if (docs) {
          deferred.resolve(docs);
        }
      }
    });
  
  return deferred.promise;
}


function getFileDetails(searchDetails) {
  var deferred = Q.defer();
  
  Q.all([
    getFileTypes(),
    getAllSearchedFiles(searchDetails)
  ])
    .then(function(results) {
      var allFileTypes = results[0];
      var allFoundFiles = results[1];
      
      var returnThis = {};
      returnThis.allFoundFiles = {};
      returnThis.groupedFileTypes = {};
      returnThis.fileCategory = {};
      
      async.each(allFoundFiles, function(foundFile, foundFileCallback) {
        returnThis.allFoundFiles[foundFile._id] = foundFile;
        
        async.each(fileGroups, function(checkFileGroup, fileGroupCallback) {
          if (foundFile[checkFileGroup]) {
            if (!returnThis.fileCategory[checkFileGroup]) {
              returnThis.fileCategory[checkFileGroup] = {};
            }
            if (!returnThis.fileCategory[checkFileGroup][foundFile[checkFileGroup]]) {
              returnThis.fileCategory[checkFileGroup][foundFile[checkFileGroup]] = {};
              returnThis.fileCategory[checkFileGroup][foundFile[checkFileGroup]].allFiles = [];
            }
            returnThis.fileCategory[checkFileGroup][foundFile[checkFileGroup]].allFiles.push(foundFile._id);
            
            
            if (foundFile.fileType) {
              if (!returnThis.fileCategory[checkFileGroup][foundFile[checkFileGroup]][foundFile.fileType]) {
                returnThis.fileCategory[checkFileGroup][foundFile[checkFileGroup]][foundFile.fileType] = {};
                returnThis.fileCategory[checkFileGroup][foundFile[checkFileGroup]][foundFile.fileType].files = [];
                returnThis.fileCategory[checkFileGroup][foundFile[checkFileGroup]][foundFile.fileType].fileTypeDetails = allFileTypes[foundFile.fileType];
              }
              returnThis.fileCategory[checkFileGroup][foundFile[checkFileGroup]][foundFile.fileType].files.push(foundFile._id);
            }
          }
          fileGroupCallback();
        }, function(err) {
          foundFileCallback();
        });
      }, function(err) {
        deferred.resolve(returnThis);
      });
    })
  
  return deferred.promise;
}




function findFilesDetailed(req, res) {
  getFileDetails(req.body)
    .then(function(returnThis) {
      res.json(returnThis);
    })
}


function findFileGroups(req, res) {
  Files.find(req.body)
    .exec(function (err, docs) {
      if (err) {
        res.status(500).json(err);
      } else {
        if (docs) {
        
        } else {
          res.status(400).json({message: "unknown"})
        }
      }
    })
}


function fileCount(req, res) {
  Files.count(req.body)
    .exec(function (err, count) {
      if (err) {
        res.status(500).json(err);
      } else {
        res.json(count);
      }
    })
}

function update(req, res) {
  thisFile = req.body;
  Files.findOneAndUpdate({_id: thisFile._id}, thisFile, function (err, doc) {
    if (err) {
      console.log("error updating File", err);
      res.json(err);
    } else {
      res.json(doc);
    }
  });
}




/**
 * CLEAN UP OLD FILE MODEL
 * STORE ALL FILE ATTRIBUTES IN THE FILES MODEL, THEN CREATE LINKS AS ASSOCIATED FILES WITHIN EACH MODEL.
 */

var models = {
  pages: require('../models/Pages').model,
  contentBlocks: require('../models/ContentBlocks').model,
  land: require('../models/Land').model,
  package: require('../models/Packages').model
}
var modelsKeys = Object.keys(models);



function updateFileLinks(req, res) {
  Files.find({})
    .exec(function (err, files) {
      console.log("files.length: ", files.length);
      var i = files.length;
      async.eachSeries(files, function(file, docsCallback) {
        var thisFile = file;
        console.log("thisFile " + i-- + ": ", thisFile._id);
        
        async.eachSeries(modelsKeys, function(key, modelKeyCallback) {
          var thisKey = key;
          console.log("thisFile[key]: ", thisFile[thisKey]);
          if (thisFile[thisKey]) {
            var addFileDetails = {
              fileId: thisFile._id,
              fileType: thisFile.fileType,
              mimetype: thisFile.mimetype,
              storeName: thisFile.storeName
            }
            console.log("thisFile[key]: ", thisFile[thisKey]);
            console.log("addFileDetails: ", addFileDetails);
            console.log("models[key]: ", thisKey);
            
            models[thisKey].findOne({_id: thisFile[thisKey]})
              .exec(function(findErr, foundDoc) {
                if (findErr) {
                  console.log("findErr: ", findErr);
                } else {
                  if (foundDoc) {
                    var updatingDoc = foundDoc;
                    console.log("updatingDoc " + i + ": ", updatingDoc._id);
                    if (!updatingDoc.associatedFiles) {
                      updatingDoc.associatedFiles = [];
                    }
                    updatingDoc.associatedFiles.push(addFileDetails);
  
                    models[thisKey].findOneAndUpdate({_id: updatingDoc._id}, updatingDoc, {returnNewDocument: true}, function (updateDocErr, updatedDoc) {
                      if (updateDocErr) {
                        console.log("error " + i + ": " + updatingDoc._id)
                        modelKeyCallback();
                      } else {
                        console.log("updated " + i + ": " + updatedDoc._id)
                        modelKeyCallback();
                      }
                    });
                  } else {
                    modelKeyCallback();
                  }
                }
              })
          } else {
            modelKeyCallback();
          }
        }, function(err) {
          docsCallback();
        })
      }, function(error) {
        console.log("complete")
      })
    })
  
  res.json({running: true})
}



function removeFileLinks(req, res) {
  async.each(modelsKeys, function(key, modelKeyCallback) {
    models[key].find({})
      .exec(function(findErr, docs) {
        if (findErr) {
          modelKeyCallback();
        } else {
          async.each(docs, function(doc, docCallback) {
            var newDoc = doc;
            newDoc.associatedFiles = [];
            
            models[key].findOneAndUpdate({_id: newDoc._id}, newDoc, {new: true}, function (updateDocErr, updatedDoc) {
              if (updateDocErr) {
                console.log("error " + newDoc._id)
              } else {
                console.log("updated " + updatedDoc._id)
              }
              docCallback();
            });
          }, function(asyncDocErr) {
            modelKeyCallback();
          })
        }
      })
  }, function(asyncModelKeysErr) {
    console.log("completed all tasks");
  })
  
  res.json({running: true})
}


/**
 * end CLEAN UP OLD FILE MODEL
 */





module.exports = {
  routes: [
    {
      path: "uploadFiles",
      method: "post",
      fn: uploadFiles,
      middleware: []
    },
    {
      path: "findAll",
      method: "post",
      fn: findallPost,
      middleware: []
    },
    {
      path: "update",
      method: "post",
      fn: update,
      middleware: []
    },
    {
      path: "findallPopulate",
      method: "post",
      fn: findallPopulate,
      middleware: []
    },
    {
      path: "fileCount",
      method: "post",
      fn: fileCount,
      middleware: []
    },
    {
      path: "findLandFiles",
      method: "post",
      fn: findLandFiles,
      middleware: []
    },
    {
      path: "findFilesDetailed",
      method: "post",
      fn: findFilesDetailed,
      middleware: []
    },
    {
      path: "updateFileLinks",
      method: "get",
      fn: updateFileLinks,
      middleware: []
    },
    {
      path: "removeFileLinks",
      method: "get",
      fn: removeFileLinks,
      middleware: []
    }
    
  ],
  getFileDetails: getFileDetails
};
