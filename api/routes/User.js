/*
 This defines custom routes. It is pretty standard nodejs, you just need to add to the routes array
 the appropriate details so a route can be created with security and other bits.

 This example overrides the default find function in the Cat model by simply using the find name.
 To override the default functions on the models use the following names:

 find,
 create,
 update,
 remove,
 findById,
 search

 */

var User = require('../models/User').model;
var middleware = require('../middleware');
var _ = require('lodash');

function login(req, res) {
    var admindata = {
        email: "james@masini.com.au",
        password: "horace",
        role_admin: true
    };
    User.update({email: admindata.email}, admindata, {upsert: true}, function (err, doc) {
        if (err) {
            console.log("get admin error", err)
        }
    });

    User.findOne({
        email: req.body.email,
        password: req.body.password
    })
        .exec(function (err, doc) {
            if (err) {
                res.status(500).json(err);
            } else {
                if (doc) {
                    res.json(doc);
                    req.session.user = doc;
                    req.session.save();
                    // console.log("save session: ", req.session);
                    // console.log("save sessionID: ", req.sessionID);
                } else {
                    res.status(400).json({message: "Username or password invalid"})
                }
            }
        })
}

function findall(req, res) {
    User.find({}, function (err, docs) {
        res.json(docs);
    })
}


function findUserById(req, res) {
    User.findOne({_id: req.body._id}, function (err, doc) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (doc) {
                res.json(doc);
            } else {
                res.status(400).json({message: "unknown user"})
            }
        }
    })
}

function findUserByTypeahead(req, res) {
    User.find({email: {"$regex": req.body.email, "$options": "i"}}, function (err, doc) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (doc) {
                res.json(doc);
            } else {
                res.status(400).json({message: "unknown user"})
            }
        }
    })
}

function remove(req, res) {
    var myID = req.body._id;
    console.log(myID);

    User.findOneAndRemove({_id: myID}, function (err, doc) {
        if (err) {
            res.json(err);
        } else {
            if (!doc) {
                res.json(404, {message: "Delete failed, record not found"});
            } else {
                console.log("user deleted");
                User.find({}, function (err, docs) {
                    res.json(docs);
                })
            }
        }
    })
}

function create(req, res) {
    var thisuser = req.body;
    console.log("add this: ", thisuser);

    if (thisuser._id) {
        User.findOneAndUpdate({_id: thisuser._id}, thisuser, function (err, doc) {
            if (err) {
                console.log("error updating user", err)
                res.json(err);
            } else {
                User.find({}, function (err, doc) {
                    res.json(doc);
                })
            }
        });
    } else {
        User.create(thisuser, function (err, doc) {
            if (err) {
                console.log("error creating user", err)
                res.json(err);
            } else {
                User.find({}, function (err, doc) {
                    res.json(doc);
                })
            }
        });
    }
}

function signup(req, res) {
    var thisuser = req.body;
    console.log("add this: ", thisuser);

    User.create(thisuser, function (err, doc) {
        if (err) {
            console.log("error creating user", err)
            res.json(err);
        } else {
            res.json(doc);
        }
    });
}


function current(req, res) {
    console.log("save session: ", req.session);
    console.log("save sessionID: ", req.sessionID);
    if (req.session.user) {
        var id = req.session.user._id;
        User.findOne({_id: id})
          .exec(function (err, doc) {
              doc.password = undefined;
              res.json(doc);
          })
    } else {
        res.status(401).json({message: "Username or password invalid"})
    }
}

function logOut(req, res){
    req.session.destroy();
    res.json({success: true});
}

module.exports = {
    routes: [
        {
            path: "login",
            method: "post",
            fn: login,
            middleware: []
        },
        {
            path: "findall",
            method: "get",
            fn: findall,
            middleware: []
        },
        {
            path: "findUserById",
            method: "post",
            fn: findUserById,
            middleware: []
        },
        {
            path: "remove",
            method: "post",
            fn: remove,
            middleware: []
        },
        {
            path: "create",
            method: "post",
            fn: create,
            middleware: []
        },
        {
            path: "signup",
            method: "post",
            fn: signup,
            middleware: []
        },
        {
            path: "current",
            method: "get",
            fn: current,
            middleware: []
        },
        {
            path: "logout",
            method: "get",
            fn: logOut,
            middleware: []
        },
        {
            path: "findUserByTypeahead",
            method: "post",
            fn: findUserByTypeahead,
            middleware: []
        }
    ]
};
