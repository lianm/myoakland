var Files = require('../models/Files').model;
var _ = require('lodash');
var Q = require('q');
var fs = require('fs');
var path = require('path');
var async = require('async');

/**
 * GOOGLE CLOUD STORAGE PARAMETERS
 **/

/*
var gcloud = require('google-cloud');
var storage = gcloud.storage;
var keyPath = path.normalize(__dirname + '/../../engine/lib/GoogleCloud-AltitudePM.json');
var gcs = storage({
  projectId: 'altitudepm-155004',
  keyFilename: keyPath
});
var bucket = gcs.bucket('clientstorage.altitudepm.com.au');
var defaultFolder = 'oaklandestate/';
*/

var keyPath = path.normalize(__dirname + '/../../engine/lib/GoogleCloud-AltitudePM.json');
var storageConfig = {
  projectId: 'altitudepm-155004',
  keyFilename: keyPath
};
var storage = require('@google-cloud/storage')(storageConfig);
var bucket = storage.bucket('clientstorage.altitudepm.com.au');
var defaultFolder = 'oaklandestate/';

/**
 * END GOOGLE CLOUD STORAGE PARAMETERS
 **/





/**
 * MULTER (FILE HANDLING) PARAMETERS - INC. FILE NAME SETUP
 **/
var Multer  = require('multer');
var multer = Multer({
  storage: Multer.memoryStorage(),
  limits: {
    fileSize: 5 * 1024 * 1024 // no larger than 5mb, you can change as needed.
  }
});



/**
 * FILE UPLOADER
 */
function fileUpload(req, res) {
  multer.single('file')(req, res, function (err) {
    if (err) {
      res.status(400).send('multer error.');
      return;
    }
    
    if (!req.file) {
      res.status(400).send('No file uploaded.');
      return;
    }
  
    renameFile(req.file.originalname)
      .then(function(newFileName) {
        var finalDestination = defaultFolder + "unknown/" + newFileName;
        if (req.body.directory) {
          finalDestination = defaultFolder + req.body.directory + newFileName;
        }
      
        // Create a new blob in the bucket and upload the file data.
        var blob = bucket.file(finalDestination);
        var blobStream = blob.createWriteStream();
      
        blobStream.on('error', function(blobErr) {
          console.log("err: ", blobErr);
          next(blobErr);
        });
      
        blobStream.on('finish', function() {
          // The public URL can be used to directly access the file via HTTP.
          var saveThis = {};
          saveThis.originalName = req.file.originalname;
          saveThis.mimetype = req.file.mimetype;
          saveThis.newName = newFileName;
          saveThis.storeName = blob.name;
          saveThis.metaData = blob.metadata;
        
          if (req.body.uploadType && req.body.uploadTypeId) {
            saveThis[req.body.uploadType] = req.body.uploadTypeId;
          }
        
          saveFile(saveThis)
            .then(function(savedFile) {
              if (req.body.currentAssociations) {
                var proposedAssociation = req.body.currentAssociations;
                var proposedAssociationKeys = Object.keys(proposedAssociation);
                
                async.eachSeries(proposedAssociationKeys, function(key, associationCallback) {
                  addModelAssociation(key, proposedAssociation[key], savedFile)
                    .then(function(updatedDoc) {
                      console.log("updated doc: " + updatedDoc._id);
                      associationCallback();
                    })
                }, function(err) {
                  res.status(200).send(savedFile);
                })
              } else {
                res.status(200).send(savedFile);
              }
            })
        });
      
        blobStream.end(req.file.buffer);
      })
  })
  
}


function renameFile(currentFileName) {
  var deferred = Q.defer();
  
  var oldName = currentFileName;
  var oldNameFileType = "." + oldName.substr(oldName.lastIndexOf('.')+1);
  var oldNameWithoutType = oldName.substr(0, oldName.lastIndexOf('.'));
  var newName = oldNameWithoutType.replace(/\s+/g,"_") + '_' + Date.now() + oldNameFileType;
  var newNameUrlEncoded = encodeURIComponent(newName);
  deferred.resolve(newNameUrlEncoded);
  
  return deferred.promise;
}


function saveFile(fileDetails) {
  var deferred = Q.defer();
  
  if (fileDetails._id) {
    Files.findOneAndUpdate({_id: fileDetails._id}, fileDetails, function (err, doc) {
      if (err) {
        console.log("error creating file", err);
        deferred.resolve(err);
      } else {
        deferred.resolve(doc);
      }
    });
  } else {
    Files.create(fileDetails, function (err, doc) {
      if (err) {
        console.log("error creating file", err);
        deferred.resolve(err);
      } else {
        deferred.resolve(doc);
      }
    });
  }
  
  return deferred.promise;
}


/**
 * END FILE UPLOADER
 */




/**
 * SAVE FILE TO MODEL
 */

var models = {
  pages: require('../models/Pages').model,
  contentBlocks: require('../models/ContentBlocks').model,
  land: require('../models/Land').model,
  package: require('../models/Packages').model,
  builders: require('../models/Builders').model
}

function addModelAssociation(model, itemId, fileDetails) {
  var deferred = Q.defer();
  
  var addFileDetails = {
    fileId: fileDetails._id,
    fileType: fileDetails.fileType,
    mimetype: fileDetails.mimetype,
    storeName: fileDetails.storeName
  }
  models[model].findOne({_id: itemId})
    .exec(function(findErr, foundDoc) {
      if (findErr) {
        console.log("saveModelAssociation findErr: ", findErr);
      } else {
        if (foundDoc) {
          var updatingDoc = foundDoc;
          if (!updatingDoc.associatedFiles) {
            updatingDoc.associatedFiles = [];
          }
          updatingDoc.associatedFiles.push(addFileDetails);
          
          models[model].findOneAndUpdate({_id: updatingDoc._id}, updatingDoc, {returnNewDocument: true}, function (updateDocErr, updatedDoc) {
            if (updateDocErr) {
              console.log("saveModelAssociation updateDocErr: ", updateDocErr);
            } else {
              console.log("document updated: " + updatedDoc._id);
              deferred.resolve(updatedDoc);
            }
          });
        } else {
          console.log("saveModelAssociation - no documents found");
        }
      }
    })
  
  return deferred.promise;
}
/**
 * END SAVE FILE TO MODEL
 */



function update(req, res) {
  saveFile(req.body)
    .then(function(saveResults) {
      res.json(saveResults);
    })
}


function findallPost(req, res) {
  Files.find(req.body)
    .exec(function (err, docs) {
      if (err) {
        res.status(500).json(err);
      } else {
        if (docs) {
          res.json(docs);
        } else {
          res.status(400).json({message: "unknown"})
        }
      }
    })
}


module.exports = {
  routes: [
    {
      path: "fileUpload",
      method: "post",
      fn: fileUpload,
      middleware: []
    },
    {
      path: "findAll",
      method: "post",
      fn: findallPost,
      middleware: []
    },
    {
      path: "update",
      method: "post",
      fn: update,
      middleware: []
    }
  ],
};
