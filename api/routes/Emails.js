var Emails = require('../models/Emails').model;
var middleware = require('../middleware');
var _ = require('lodash');
var Q = require('q');
var async = require('async');


var emailDetails = {
  fromAddress: 'Oakland@altitudepm.com.au',
  fromName: 'Oakland Estate - Altitude Property Marketing',
  subject: 'Oakland Estate - Website Enquiry',
  defaultTemplate: 'oaklandestate-general'
};

var mandrill = require('mandrill-api/mandrill');
var mandrill_client = new mandrill.Mandrill('zZgLDyP-JF_5CUNtIx-m5Q');



function findall(req, res) {
  Emails.find({}, function (err, docs) {
    res.json(docs);
  })
}



function findPage(req, res) {
  Emails.find(req.body, function (err, docs) {
    res.json(docs);
  })
}


function remove(req, res) {
  var myID = req.body._id;
  console.log("myID: " + myID);
  
  Emails.findOneAndRemove({_id: myID}, function (err, doc) {
    if (err) {
      res.json(err);
    } else {
      if (!doc) {
        res.json(404, {message: "Delete failed, record not found"});
      } else {
        console.log("user deleted");
        Emails.find({}, function (err, docs) {
          res.json(docs);
        })
      }
    }
  })
}

function create(req, res) {
  save(req.body)
    .then(function(savedEmail) {
      res.json(savedEmail);
    })
}





function send(req, res) {
  Q.all([
    save(req.body),
    emailCustomer(req.body),
    emailAdmin(req.body)
  ])
    .then(function(qResults){
      req.session.enquiry = qResults[0];
      req.session.save();
  
      res.json({
        session: req.session,
        savedEmail: qResults[0],
        emailResult: qResults[1]
      });
    })
}

function emailCustomer(enquiryDetails) {
  var deferred = Q.defer();
  
  var customSendParams = {
    to: [],
    global_merge_vars: []
  };
  
  if (enquiryDetails.name && enquiryDetails.email) {
    customSendParams.to.push({
      "email": enquiryDetails.email,
      "name": enquiryDetails.name,
      "type": "to"
    });
    customSendParams.global_merge_vars.push({
      "name": "name",
      "content": enquiryDetails.name
    });
  }
  
  if (enquiryDetails.additionalFormData.customEmailContent) {
    customSendParams.global_merge_vars.push({
      "name": "customEmailContent",
      "content": enquiryDetails.additionalFormData.customEmailContent
    })
  }
  
  if (enquiryDetails.additionalFormData.preserve_recipients) {
    customSendParams.preserve_recipients = enquiryDetails.additionalFormData.preserve_recipients;
  }
  
  if (enquiryDetails.additionalFormData.subject) {
    customSendParams.subject = enquiryDetails.additionalFormData.subject;
  }
  
  if (enquiryDetails.template_name) {
    customSendParams.template_name = enquiryDetails.template_name;
  }
  
  mandrillSend(customSendParams)
    .then( function(sendResult) {
      deferred.resolve(sendResult);
    });
  
  return deferred.promise;
}



function emailAdmin(enquiryDetails) {
  var deferred = Q.defer();
  
  var customSendParams = {
    to: [],
    global_merge_vars: []
  };
  
  var emailContent = '<p><strong>Customer Enquiry Details</strong></p>';
  emailContent += '<p>Name: ' + enquiryDetails.name +'</p>';
  emailContent += '<p>Phone: ' + enquiryDetails.phone +'</p>';
  emailContent += '<p>Email: ' + enquiryDetails.email +'</p>';
  emailContent += '<p>Message:<br/>' + enquiryDetails.message +'</p>';
  emailContent += '<br class="break"/>';
  emailContent += '<p>' + enquiryDetails.additionalFormData.customEmailContent +'</p>';
  
  customSendParams.template_name = "altitude-enquiry";
  customSendParams.global_merge_vars.push({
    "name": "customEmailContent",
    "content": emailContent
  });
  
  customSendParams.subject = 'Oakland Estate Website Enquiry - ' + enquiryDetails.email;
  if (enquiryDetails.additionalFormData.adminSubject) {
    customSendParams.subject = enquiryDetails.additionalFormData.adminSubject;
  }
  
  mandrillSend(customSendParams)
    .then( function(sendResult) {
      deferred.resolve(sendResult);
    });
  
  return deferred.promise;
}



function mandrillSend(customSendParams) {
  var deferred = Q.defer();
  
  buildSendParams(customSendParams)
    .then(function(sendParams) {
      console.log("sendParams: ", sendParams);
      mandrill_client.messages.sendTemplate(sendParams, function (result) {
        deferred.resolve(result);
      }, function (err) {
        console.log("mandrillSend error: ", err);
        deferred.resolve({error: true, errorMessage: err});
      });
    })
  
  return deferred.promise;
}


function buildSendParams(customSendParams) {
  var deferred = Q.defer();
  
  var sendParams = {
    "template_name": emailDetails.defaultTemplate,
    "template_content": [],
    "message": {
      // "html": "<p>Example HTML content</p>",
      // "text": "Example text content",
      // "subject": "example subject",
      // "from_email": emailDetails.fromAddress,
      // "from_name": emailDetails.fromName,
      "to": [{
        "email": emailDetails.fromAddress,
        "name": emailDetails.fromName,
        "type": "cc"
      }],
      "preserve_recipients": true,
      "auto_text": true,
      "merge": true,
      "merge_language": "handlebars",
      "global_merge_vars": []
    }
  };
  
  if (customSendParams.template_name) {
    sendParams.template_name = customSendParams.template_name;
  }
  
  if (customSendParams.to) {
    sendParams.message.to = _.union(sendParams.message.to,customSendParams.to);
  }
  
  if (customSendParams.preserve_recipients) {
    sendParams.message.preserve_recipients = customSendParams.preserve_recipients;
  }
  
  if (customSendParams.global_merge_vars) {
    sendParams.message.global_merge_vars = _.union(sendParams.message.global_merge_vars,customSendParams.global_merge_vars);
  }
  
  if (customSendParams.subject) {
    sendParams.message.subject = customSendParams.subject;
  }
  
  deferred.resolve(sendParams);
  
  return deferred.promise;
}


function save(saveContent) {
  var deferred = Q.defer();
  
  if (saveContent._id) {
    Emails.findOneAndUpdate({_id: saveContent._id}, saveContent, function (err, doc) {
      if (err) {
        deferred.resolve({"error": true, "errorMessage": err});
      } else {
        deferred.resolve(doc);
      }
    });
  } else {
    Emails.create(saveContent, function (err, doc) {
      if (err) {
        deferred.resolve({"error": true, "errorMessage": err});
        res.json(err);
      } else {
        deferred.resolve(doc);
      }
    });
  }
  
  return deferred.promise;
}


module.exports = {
  routes: [
    {
      path: "findall",
      method: "get",
      fn: findall,
      middleware: []
    },
    {
      path: "create",
      method: "post",
      fn: create,
      middleware: []
    },
    {
      path: "send",
      method: "post",
      fn: send,
      middleware: []
    },
    {
      path: "findPage",
      method: "post",
      fn: findPage,
      middleware: []
    },
    {
      path: "remove",
      method: "post",
      fn: remove,
      middleware: []
    }
  ]
};
