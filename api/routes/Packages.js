var Packages = require('../models/Packages').model;
var middleware = require('../middleware');
var _ = require('lodash');
var Q = require('q');
var async = require('async');
var Files = require('../routes/Files');


function findall(req, res) {
  console.log("save session: ", req.session);
  console.log("save sessionID: ", req.sessionID);
  Packages.find({}, function (err, docs) {
    res.json(docs);
  })
}

function findallPopulate(req, res) {
  Packages.find(req.body)
    .populate(['builder', 'land', 'home'])
    .exec(function (err, docs) {
      if (err) {
        console.log("err: ", err);
        res.json({error: true, errorMessage: err})
      } else if (docs.length == 0) {
        console.log("NO DOCUMENTS FOUND");
        res.json({error: true, errorMessage: "NO DOCUMENTS FOUND"})
      } else {
        res.json(docs);
      }
    })
}

function findPackage(req, res) {
  Packages.find(req.body)
    .exec(function(err, landResults) {
      if (err) {
        res.json({error: true, errorMessage: err})
      } else {
        res.json(landResults);
      }
    })
}


function remove(req, res) {
  var myID = req.body._id;
  console.log(myID);
  
  Packages.findOneAndRemove({_id: myID}, function (err, doc) {
    if (err) {
      res.json(err);
    } else {
      if (!doc) {
        res.json(404, {message: "Delete failed, record not found"});
      } else {
        console.log("user deleted");
        Packages.find({}, function (err, docs) {
          res.json(docs);
        })
      }
    }
  })
}

function removeMany(req, res) {
  Packages.remove(req.body, function (err, doc) {
    if (err) {
      res.json(err);
    } else {
      if (!doc) {
        res.json(404, {message: "Delete failed, record not found"});
      } else {
        console.log("user deleted");
        Packages.find({}, function (err, docs) {
          res.json(docs);
        })
      }
    }
  })
}

function create(req, res) {
  var thisuser = req.body;
  console.log("add this: ", thisuser);
  
  if (thisuser._id) {
    Packages.findOneAndUpdate({_id: thisuser._id}, thisuser, function (err, doc) {
      if (err) {
        console.log("error updating user", err);
        res.json(err);
      } else {
        Packages.find({}, function (err, doc) {
          res.json(doc);
        })
      }
    });
  } else {
    Packages.create(thisuser, function (err, doc) {
      if (err) {
        console.log("error creating user", err);
        res.json(err);
      } else {
        Packages.find({}, function (err, doc) {
          res.json(doc);
        })
      }
    });
  }
}


function packageScroller() {
  var deferred = Q.defer();
  
  findPackagePopulate({})
    .then(function(packageResults) {
      var totalResults = 2;
      var returnResults = [];
      for (var i = 0; i < totalResults; i++) {
        returnResults.push(packageResults[Math.floor(Math.random() * packageResults.length)]);
    
        if (i == totalResults -1) {
          deferred.resolve(returnResults);
        }
      }
    })
  
  return deferred.promise;
}



function findPackages(searchThis) {
  var deferred = Q.defer();
  
  searchParameters(searchThis)
    .then(function(packageSearch) {
      console.log("packageSearch: ", packageSearch);
  
      findPackagePopulate(packageSearch)
        .then(function(packageResults) {
          var returnThis = {
            packageSearch: packageSearch,
            packageDetails: packageResults
          };
          deferred.resolve(returnThis);
        })
    });

  return deferred.promise;
}


function searchParameters(searchThis) {
  var deferred = Q.defer();
  
  console.log("package query: ", searchThis);
  var packageSearch  = {};
  // PACKAGE PRICE
  if (searchThis.packagePrice || searchThis.packagePriceGte || searchThis.packagePriceLte) {
    if (searchThis.packagePrice) {
      packageSearch.packagePrice = searchThis.packagePrice;
    } else {
      packageSearch.packagePrice = {};
      if (searchThis.packagePriceGte) {
        packageSearch.packagePrice.$gte = searchThis.packagePriceGte;
      }
      if (searchThis.packagePriceLte) {
        packageSearch.packagePrice.$lte = searchThis.packagePriceLte;
      }
    }
  }
  
  // BEDROOMS
  if (searchThis.bedrooms || searchThis.bedroomsGte || searchThis.bedroomsLte) {
    if (searchThis.bedrooms) {
      packageSearch.bedrooms = searchThis.bedrooms;
    } else {
      packageSearch.bedrooms = {};
      if (searchThis.bedroomsGte) {
        packageSearch.bedrooms.$gte = searchThis.bedroomsGte;
      }
      if (req.body.bedroomsLte) {
        packageSearch.bedrooms.$lte = searchThis.bedroomsLte;
      }
    }
  }
  
  // BATHROOMS
  if (searchThis.bathrooms || searchThis.bathroomsGte || searchThis.bathroomsLte) {
    if (searchThis.bathrooms) {
      packageSearch.bathrooms = searchThis.bathrooms;
    } else {
      packageSearch.bathrooms = {};
      if (searchThis.bathroomsGte) {
        packageSearch.bathrooms.$gte = searchThis.bathroomsGte;
      }
      if (searchThis.bathroomsLte) {
        packageSearch.bathrooms.$lte = searchThis.bathroomsLte;
      }
    }
  }
  
  // CARS
  if (searchThis.cars || searchThis.carsGte || searchThis.carsLte) {
    if (searchThis.cars) {
      packageSearch.cars = searchThis.cars;
    } else {
      packageSearch.cars = {};
      if (searchThis.carsGte) {
        packageSearch.cars.$gte = searchThis.carsGte;
      }
      if (searchThis.carsLte) {
        packageSearch.cars.$lte = searchThis.carsLte;
      }
    }
  }
  
  // HOUSE SIZE
  if (searchThis.houseSize || searchThis.houseSizeGte || searchThis.houseSizeLte) {
    if (searchThis.houseSize) {
      packageSearch.houseSize = searchThis.houseSize;
    } else {
      packageSearch.houseSize = {};
      if (searchThis.houseSizeGte) {
        packageSearch.houseSize.$gte = searchThis.houseSizeGte;
      }
      if (searchThis.houseSizeLte) {
        packageSearch.houseSize.$lte = searchThis.houseSizeLte;
      }
    }
  }
  
  // HOUSE LEVELS
  if (searchThis.houseLevels) {
    packageSearch.houseLevels = searchThis.houseLevels;
  }
  
  // BUILDER
  if (searchThis.builder) {
    packageSearch.builder = searchThis.builder;
  }
  
  // LAND
  if (searchThis.land) {
    packageSearch.land = searchThis.land;
  }
  
  // Lot Size
  if (searchThis.landSizeGte || searchThis.landSizeLte) {
    packageSearch.landSize = {};
    if (searchThis.landSizeGte) {
      packageSearch.landSize.$gte = searchThis.landSizeGte;
    }
    if (searchThis.landSizeLte) {
      packageSearch.landSize.$lte = searchThis.landSizeLte;
    }
  }
  
  deferred.resolve(packageSearch);

  return deferred.promise;
}


function findPackagePopulate(packageSearch) {
    var deferred = Q.defer();

    Packages.find(packageSearch)
        .populate(['builder', 'land'])
        .exec(function (err, packageResults) {
            deferred.resolve(packageResults);
        })

    return deferred.promise;
}

function countBuilders() {
  var deferred = Q.defer();
  
  Packages.aggregate([
    {
      $group: {
        _id: '$builder',
        count: { $sum: 1 }
      }
    }
  ])
    .exec(function (err, countResults) {
      if (err) {
        deferred.resolve(err);
      } else {
        if (countResults) {
          if (countResults.length > 0) {
            var returnThis = {};
            for (var i = 0; i < countResults.length; i++) {
              returnThis[countResults[i]._id] = countResults[i];
              if (i == countResults.length -1) {
                deferred.resolve(returnThis);
              }
            }
          } else {
            deferred.resolve(countResults);
          }
        } else {
          deferred.resolve('No countResults Results');
        }
      }
    })
  
  return deferred.promise;
}


module.exports = {
  routes: [
    {
      path: "findall",
      method: "get",
      fn: findall,
      middleware: []
    },
    {
      path: "remove",
      method: "post",
      fn: remove,
      middleware: []
    },
    {
      path: "create",
      method: "post",
      fn: create,
      middleware: []
    },
    {
      path: "findPackage",
      method: "post",
      fn: findPackage,
      middleware: []
    },
    {
      path: "removeMany",
      method: "post",
      fn: removeMany,
      middleware: []
    },
    {
      path: "findallPopulate",
      method: "post",
      fn: findallPopulate,
      middleware: []
    }
  ],
  findPackages: findPackages,
  countBuilders: countBuilders,
  packageScroller: packageScroller
};
