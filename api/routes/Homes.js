var Homes = require('../models/Homes').model;
var middleware = require('../middleware');
var _ = require('lodash');
var Q = require('q');
var async = require('async');


function findall(req, res) {
  Homes.find(req.body)
    .exec(function (err, docs) {
      if (err) {
        console.log("err: ", err);
        res.json({error: true, errorMessage: err})
      } else if (docs.length == 0) {
        console.log("NO DOCUMENTS FOUND");
        res.json({error: true, errorMessage: "NO DOCUMENTS FOUND"})
      } else {
        res.json(docs);
      }
    })
}

function findallPopulate(req, res) {
  Homes.find(req.body)
    .populate(['builder'])
    .exec(function (err, docs) {
      if (err) {
        console.log("err: ", err);
        res.json({error: true, errorMessage: err})
      } else if (docs.length == 0) {
        console.log("NO DOCUMENTS FOUND");
        res.json({error: true, errorMessage: "NO DOCUMENTS FOUND"})
      } else {
        res.json(docs);
      }
    })
}


function remove(req, res) {
  var myID = req.body._id;
  console.log("myID: ", myID);
  
  Homes.findOneAndRemove({_id: myID}, function (err, doc) {
    if (err) {
      console.log("error: ", err);
      res.json({error: true, errorMessage: err});
    } else {
      if (!doc) {
        console.log("NO DOCUMENTS FOUND");
        res.json({error: true, errorMessage: "NO DOCUMENTS FOUND"})
      } else {
        console.log("user deleted");
        Homes.find({}, function (err, docs) {
          res.json(docs);
        })
      }
    }
  })
}


function removeMany(req, res) {
  Homes.remove(req.body, function (err, doc) {
    if (err) {
      res.json(err);
    } else {
      if (!doc) {
        res.json(404, {message: "Delete failed, record not found"});
      } else {
        console.log("user deleted");
        Homes.find({}, function (err, docs) {
          res.json(docs);
        })
      }
    }
  })
}

function create(req, res) {
  var thisuser = req.body;
  console.log("add this: ", thisuser);
  
  if (thisuser._id) {
    Homes.findOneAndUpdate({_id: thisuser._id}, thisuser, function (err, doc) {
      if (err) {
        console.log("error updating user", err);
        res.json(err);
      } else {
        res.json(doc);
      }
    });
  } else {
    Homes.create(thisuser, function (err, doc) {
      if (err) {
        console.log("error creating user", err);
        res.json(err);
      } else {
        res.json(doc);
      }
    });
  }
}



module.exports = {
  routes: [
    {
      path: "findall",
      method: "post",
      fn: findall,
      middleware: []
    },
    {
      path: "remove",
      method: "post",
      fn: remove,
      middleware: []
    },
    {
      path: "create",
      method: "post",
      fn: create,
      middleware: []
    },
    {
      path: "findallPopulate",
      method: "post",
      fn: findallPopulate,
      middleware: []
    },
    {
      path: "removeMany",
      method: "post",
      fn: removeMany,
      middleware: []
    }
  ]
};
