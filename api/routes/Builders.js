var Builders = require('../models/Builders').model;
var middleware = require('../middleware');
var _ = require('lodash');
var Q = require('q');
var async = require('async');



function findall(req, res) {
  console.log("save session: ", req.session);
  console.log("save sessionID: ", req.sessionID);
  Builders.find({})
    .sort('name')
    .exec(function (err, docs) {
      res.json(docs);
    })
}

function findallPost(req, res) {
  var searchThis = {};
  if (req.body) {
    searchThis = req.body;
  }
  Builders.find(searchThis)
    .sort('name')
    .exec(function (err, docs) {
      res.json(docs);
    })
}


function remove(req, res) {
  var myID = req.body._id;
  console.log(myID);
  
  Builders.findOneAndRemove({_id: myID}, function (err, doc) {
    if (err) {
      res.json(err);
    } else {
      if (!doc) {
        res.json(404, {message: "Delete failed, record not found"});
      } else {
        console.log("user deleted");
        Builders.find({}, function (err, docs) {
          res.json(docs);
        })
      }
    }
  })
}

function create(req, res) {
  var thisuser = req.body;
  console.log("add this: ", thisuser);
  
  if (thisuser._id) {
    Builders.findOneAndUpdate({_id: thisuser._id}, thisuser, {new: true}, function (err, doc) {
      if (err) {
        console.log("error updating user", err);
        res.json(err);
      } else {
        res.json(doc);
      }
    });
  } else {
    Builders.create(thisuser, function (err, doc) {
      if (err) {
        console.log("error creating user", err);
        res.json(err);
      } else {
        res.json(doc);
      }
    });
  }
}

var Packages = require('./Packages');

function findBuilders(searchThis) {
  var deferred = Q.defer();
  
  Q.all([
    searchBuilders(searchThis),
    Packages.countBuilders()
  ])
    .then(function(qResults) {
      var builderResults = qResults[0];
      var packageCount = qResults[1];
      if (builderResults.length > 0) {
        var returnThis = [];
        for (var i = 0; i < builderResults.length; i++) {
          var addThis = builderResults[i].toObject();
          if (packageCount[builderResults[i]._id]) {
            addThis.packageCount = packageCount[builderResults[i]._id].count;
          }
          returnThis.push(addThis);
          
          if (i == builderResults.length -1) {
            console.log("findBuilders: ", returnThis);
            deferred.resolve(returnThis);
          }
        }
      } else {
        deferred.resolve(builderResults);
      }
    });
  
  return deferred.promise;
}


function searchBuilders(searchThis) {
  var deferred = Q.defer();
  
  Builders.find(searchThis)
    .sort('name')
    .exec(function (err, buildersResults) {
      if (err) {
        deferred.resolve(err);
      } else {
        if (buildersResults) {
          deferred.resolve(buildersResults);
        } else {
          deferred.resolve('No findBuilders Results');
        }
      }
    });
  
  return deferred.promise;
}


function getBuildersSearchRange() {
  var deferred = Q.defer();
  
  Builders.find()
    .sort('name')
    .exec(function (err, buildersResults) {
      if (err) {
        deferred.resolve(err);
      } else {
        if (buildersResults) {
          if (buildersResults.length > 0) {
            var buildersSearchRange = [];
            buildersSearchRange.push({text: 'ANY', value: null});
            
            for (var i = 0; i < buildersResults.length; i++) {
              buildersSearchRange.push({text: buildersResults[i].name, value: buildersResults[i]._id});
              
              if (i === buildersResults.length -1) {
                deferred.resolve(buildersSearchRange);
              }
            }
          } else {
            deferred.resolve('No getBuildersSearchRange Results');
          }
        } else {
          deferred.resolve('unknown error');
        }
      }
    })
  
  return deferred.promise;
}


module.exports = {
  routes: [
    {
      path: "findall",
      method: "get",
      fn: findall,
      middleware: []
    },
    {
      path: "findall",
      method: "post",
      fn: findallPost,
      middleware: []
    },
    {
      path: "remove",
      method: "post",
      fn: remove,
      middleware: []
    },
    {
      path: "create",
      method: "post",
      fn: create,
      middleware: []
    },
    {
      path: "create",
      method: "post",
      fn: create,
      middleware: []
    }
  ],
  findBuilders: findBuilders,
  getBuildersSearchRange: getBuildersSearchRange
};
