var Pages = require('../models/ContentBlockTypes').model;
var middleware = require('../middleware');
var _ = require('lodash');
var Q = require('q');
var async = require('async');



function findall(req, res) {
    console.log("save session: ", req.session);
    console.log("save sessionID: ", req.sessionID);
    Pages.find({}, function (err, docs) {
        res.json(docs);
    })
}


function remove(req, res) {
    var myID = req.body._id;
    console.log(myID);

    Pages.findOneAndRemove({_id: myID}, function (err, doc) {
        if (err) {
            res.json(err);
        } else {
            if (!doc) {
                res.json(404, {message: "Delete failed, record not found"});
            } else {
                console.log("user deleted");
                Pages.find({}, function (err, docs) {
                    res.json(docs);
                })
            }
        }
    })
}

function create(req, res) {
    var thisuser = req.body;
    console.log("add this: ", thisuser);

    if (thisuser._id) {
        Pages.findOneAndUpdate({_id: thisuser._id}, thisuser, function (err, doc) {
            if (err) {
                console.log("error updating user", err)
                res.json(err);
            } else {
                Pages.find({}, function (err, doc) {
                    res.json(doc);
                })
            }
        });
    } else {
        Pages.create(thisuser, function (err, doc) {
            if (err) {
                console.log("error creating user", err)
                res.json(err);
            } else {
                Pages.find({}, function (err, doc) {
                    res.json(doc);
                })
            }
        });
    }
}





module.exports = {
    routes: [
        {
            path: "findall",
            method: "get",
            fn: findall,
            middleware: []
        },
        {
            path: "remove",
            method: "post",
            fn: remove,
            middleware: []
        },
        {
            path: "create",
            method: "post",
            fn: create,
            middleware: []
        }
    ]
};
