/*
 This defines custom routes. It is pretty standard nodejs, you just need to add to the routes array
 the appropriate details so a route can be created with security and other bits.

 This example overrides the default find function in the Cat model by simply using the find name.
 To override the default functions on the models use the following names:

 find,
 create,
 update,
 remove,
 findById,
 search

 */

var Products = require('../models/Products').model;
var middleware = require('../middleware');
var _ = require('lodash');

function findall(req, res) {
    Products.find().sort({displayOrder: 1}).exec(function(err, posts) {
        if (err) {
            console.log("find error: ", err);
        } else {
            console.log("found projects");
            res.json(posts);
        }
    });
}

function remove(req, res) {
    var myID = req.body._id;
    console.log(myID);

    Products.findOneAndRemove({_id: myID}, function (err, doc) {
        if (err) {
            res.json(err);
        } else {
            if (!doc) {
                res.json(404, {message: "Delete failed, record not found"});
            } else {
                console.log("user deleted");
                Products.find({}, function (err, docs) {
                    res.json(docs);
                })
            }
        }
    })
}


function create(req, res) {
    var thisuser = req.body;
    console.log("add this: ", thisuser);

    if (thisuser._id) {
        Products.findOneAndUpdate({_id: thisuser._id}, thisuser, function (err, doc) {
            if (err) {
                console.log("error updating user", err)
                res.json(err);
            } else {
                Products.find({}, function (err, doc) {
                    res.json(doc);
                })
            }
        });
    } else {
        Products.create(thisuser, function (err, doc) {
            if (err) {
                console.log("error creating user", err)
                res.json(err);
            } else {
                Products.find({}, function (err, doc) {
                    res.json(doc);
                })
            }
        });
    }
}

module.exports = {
    routes: [
        {
            path: "findall",
            method: "get",
            fn: findall,
            middleware: []
        },
        {
            path: "remove",
            method: "post",
            fn: remove,
            middleware: []
        },
        {
            path: "create",
            method: "post",
            fn: create,
            middleware: []
        }
    ]
};
