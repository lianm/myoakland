var Menus = require('../models/Menus').model;
var middleware = require('../middleware');
var _ = require('lodash');
var Q = require('q');
var async = require('async');



function findall(req, res) {
  console.log("save session: ", req.session);
  console.log("save sessionID: ", req.sessionID);
  Menus.find({})
    .exec(function (err, docs) {
      res.json(docs);
    })
}


function getMenus() {
  var deferred = Q.defer();
  
  Menus.find()
    .exec(function (err, docs) {
      if (err) {
        deferred.resolve({error: true, errorMessage: err});
      } else {
        if (docs) {
          var returnThis = {
            byType: {},
            byId: {}
          };
          for (var i = 0; i < docs.length; i++) {
            returnThis.byType[docs[i].menuId] = docs[i];
            returnThis.byId[docs[i]._id] = docs[i];
    
            if (i == docs.length -1) {
              deferred.resolve(returnThis);
            }
          }
        } else {
          deferred.resolve({error: true, errorMessage: "No Menu Items Available"});
        }
      }
    });
  
  return deferred.promise;
}


function remove(req, res) {
  var myID = req.body._id;
  console.log(myID);
  
  Menus.findOneAndRemove({_id: myID}, function (err, doc) {
    if (err) {
      res.json(err);
    } else {
      if (!doc) {
        res.json(404, {message: "Delete failed, record not found"});
      } else {
        console.log("user deleted");
        Menus.find({}, function (err, docs) {
          res.json(docs);
        })
      }
    }
  })
}

function create(req, res) {
  var thisuser = req.body;
  console.log("add this: ", thisuser);
  
  if (thisuser._id) {
    Menus.findOneAndUpdate({_id: thisuser._id}, thisuser, function (err, doc) {
      if (err) {
        console.log("error updating user", err)
        res.json(err);
      } else {
        Menus.find({}, function (err, doc) {
          res.json(doc);
        })
      }
    });
  } else {
    Menus.create(thisuser, function (err, doc) {
      if (err) {
        console.log("error creating user", err)
        res.json(err);
      } else {
        Menus.find({}, function (err, doc) {
          res.json(doc);
        })
      }
    });
  }
}





module.exports = {
  getMenus: getMenus,
  routes: [
    {
      path: "findall",
      method: "get",
      fn: findall,
      middleware: []
    },
    {
      path: "remove",
      method: "post",
      fn: remove,
      middleware: []
    },
    {
      path: "create",
      method: "post",
      fn: create,
      middleware: []
    }
  ]
};
