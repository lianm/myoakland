var Homepage = require('../models/Homepage').model;
var middleware = require('../middleware');
var _ = require('lodash');
var Q = require('q');
var async = require('async');



function findall(req, res) {
    Homepage.find({}, function (err, docs) {
        res.json(docs);
    })
}

function create(req, res) {
    var thisuser = req.body;
    console.log("add this: ", thisuser);

    if (thisuser._id) {
        Homepage.findOneAndUpdate({_id: thisuser._id}, thisuser, function (err, doc) {
            if (err) {
                console.log("error updating user", err)
                res.json(err);
            } else {
                Homepage.find({}, function (err, doc) {
                    res.json(doc);
                })
            }
        });
    } else {
        Homepage.create(thisuser, function (err, doc) {
            if (err) {
                console.log("error creating user", err)
                res.json(err);
            } else {
                Homepage.find({}, function (err, doc) {
                    res.json(doc);
                })
            }
        });
    }
}





module.exports = {
    routes: [
        {
            path: "findall",
            method: "get",
            fn: findall,
            middleware: []
        },
        {
            path: "create",
            method: "post",
            fn: create,
            middleware: []
        }
    ]
};
