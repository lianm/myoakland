var Land = require('../models/Land').model;
var middleware = require('../middleware');
var _ = require('lodash');
var Q = require('q');
var async = require('async');


function findland(req, res) {
  Land.find(req.body)
    .exec(function(err, landResults) {
      if (err) {
        res.json({error: true, errorMessage: err})
      } else {
        res.json(landResults);
      }
    })
}


function findall(req, res) {
  Land.find({})
    .exec(function(err, landResults) {
      if (err) {
        res.json({error: true, errorMessage: err})
      } else {
        res.json(landResults);
      }
    })
}

function lotSearch(req, res) {
  landSearch(req.body)
    .then(function(returnThis) {
      res.json(returnThis.landDetails);
    })
}

function landScroller() {
  var deferred = Q.defer();
  
  var searchCriteria = {};
  searchCriteria.sold = {$ne: true};  // important for live website. Do not return Sold lots.
  Land.find(searchCriteria)
    .exec(function (err, landArray) {
      if (err) {
        deferred.resolve({error: true, errorMessage: err})
      } else {
        var totalResults = 3;
        var returnResults = [];
        for (var i = 0; i < totalResults; i++) {
          returnResults.push(landArray[Math.floor(Math.random() * landArray.length)]);
          
          if (i == totalResults -1) {
            deferred.resolve(returnResults);
          }
        }
      }
    });
  
  return deferred.promise;
}



function landSearch(searchParams) {
  var deferred = Q.defer();
  
  console.log("searchParams: ", searchParams);
  
  searchParameters(searchParams)
    .then(function(lotSearch) {
      var searchCriteria = lotSearch;
      // searchCriteria.sold = {$ne: true};  // important for live website. Do not return Sold lots.
      console.log("searchCriteria: ", searchCriteria);
      Land.find(searchCriteria)
        .sort('lotNumber')
        .exec(function (err, landArray) {
          if (err) {
            deferred.resolve({error: true, errorMessage: err})
          } else {
            console.log('landArray: ', landArray);
            deferred.resolve({
              landDetails: landArray,
              lotSearch: searchCriteria
            });
          }
        })
    })
  
  return deferred.promise;
}

function searchParameters(searchParams) {
  var deferred = Q.defer();
  
  var lotSearch  = {};
  // Lot Id
  if (searchParams._id) {
    lotSearch._id = searchParams._id;
  }
  
  // Lot Number
  if (searchParams.lotNumber) {
    lotSearch.lotNumber = searchParams.lotNumber;
  }
  
  // Stage
  if (searchParams.stage) {
    lotSearch.stage = searchParams.stage;
  }
  
  // Lot Price
  if (searchParams.landPrice || searchParams.landPriceGte || searchParams.landPriceLte) {
    if (searchParams.landPrice) {
      lotSearch.landPrice = searchParams.landPrice;
    } else {
      lotSearch.landPrice = {};
      if (searchParams.landPriceGte) {
        lotSearch.landPrice.$gte = searchParams.landPriceGte;
      }
      if (searchParams.landPriceLte) {
        lotSearch.landPrice.$lte = searchParams.landPriceLte;
      }
    }
  }
  
  // Lot Size
  if (searchParams.landSizeGte || searchParams.landSizeLte) {
    lotSearch.landSize = {};
    if (searchParams.landSizeGte) {
      lotSearch.landSize.$gte = searchParams.landSizeGte;
    }
    if (searchParams.landSizeLte) {
      lotSearch.landSize.$lte = searchParams.landSizeLte;
    }
  }
  
  // Lot Frontage
  if (searchParams.frontageGte || searchParams.frontageLte) {
    lotSearch.frontage = {};
    if (searchParams.frontageGte) {
      lotSearch.frontage.$gte = searchParams.frontageGte;
    }
    if (searchParams.frontageLte) {
      lotSearch.frontage.$lte = searchParams.frontageLte;
    }
  }
  
  // Lot Depth
  if (searchParams.depthGte || searchParams.depthLte) {
    lotSearch.depth = {};
    if (searchParams.depthGte) {
      lotSearch.depth.$gte = searchParams.depthGte;
    }
    if (searchParams.depthLte) {
      lotSearch.depth.$lte = searchParams.depthLte;
    }
  }
  
  // Lot Hold
  if (searchParams.hold) {
    lotSearch.hold = searchParams.hold;
  }
  
  // Lot Sold
  if (searchParams.sold) {
    lotSearch.sold = searchParams.sold;
  }
  
  // Corner Lot
  if (searchParams.corner) {
    lotSearch.corner = searchParams.corner;
  }
  
  deferred.resolve(lotSearch);

  return deferred.promise;
}


function remove(req, res) {
  var myID = req.body._id;
  console.log(myID);
  
  Land.findOneAndRemove({_id: myID}, function (err, doc) {
    if (err) {
      res.json(err);
    } else {
      if (!doc) {
        res.json(404, {message: "Delete failed, record not found"});
      } else {
        console.log("user deleted");
        Land.find({}, function (err, docs) {
          res.json(docs);
        })
      }
    }
  })
}


function removeMany(req, res) {
  Land.remove(req.body, function (err, doc) {
    if (err) {
      res.json(err);
    } else {
      if (!doc) {
        res.json(404, {message: "Delete failed, record not found"});
      } else {
        console.log("user deleted");
        Land.find({}, function (err, docs) {
          res.json(docs);
        })
      }
    }
  })
}

function create(req, res) {
  var thisuser = req.body;
  console.log("add this: ", thisuser);
  
  if (thisuser._id) {
    Land.findOneAndUpdate({_id: thisuser._id}, thisuser, {new: true}, function (err, doc) {
      if (err) {
        console.log("error updating user", err)
        res.json(err);
      } else {
        res.json(doc);
      }
    });
  } else {
    Land.create(thisuser, function (err, doc) {
      if (err) {
        console.log("error creating user", err)
        res.json(err);
      } else {
        res.json(doc);
      }
    });
  }
}




module.exports = {
  routes: [
    {
      path: "findall",
      method: "get",
      fn: findall,
      middleware: []
    },
    {
      path: "remove",
      method: "post",
      fn: remove,
      middleware: []
    },
    {
      path: "create",
      method: "post",
      fn: create,
      middleware: []
    },
    {
      path: "lotSearch",
      method: "post",
      fn: lotSearch,
      middleware: []
    },
    {
      path: "findland",
      method: "post",
      fn: findland,
      middleware: []
    },
    {
      path: "removeMany",
      method: "post",
      fn: removeMany,
      middleware: []
    }
  ],
  landSearch: landSearch,
  landScroller: landScroller
};
