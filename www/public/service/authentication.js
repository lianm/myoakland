angular.module('admin').factory('authentication',function($rootScope, $transitions, $state, $http) {
  
  var authentication = {
    currentUser: null
  };
  
  $transitions.onBefore( { to: 'app.**' }, function(transition) {
    if (!authentication.currentUser || authentication.currentUser === null || authentication.currentUser === '') {
      $http.get("/api/user/current")
        .then(function(result){
          console.log("authentication check successful: ", result);
          authentication.currentUser = result.data;
          return true;
        }, function(err){
          console.log("authentication check error: ", err);
          $state.go('signIn');
          // $rootScope.afterSignUp = "/app/home";
          return false;
        });
    } else {
      return true;
    }
  });
	
	return authentication;
});
