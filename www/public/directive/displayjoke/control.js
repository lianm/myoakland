angular.module('admin').directive('directiveDisplayJoke', function () {
  return {
    controller: JokeController,
    link: JokeLink,
    templateUrl: 'directive/displayjoke/view.html'
  };
  
  function JokeLink(scope, iElement, iAttrs, vm) {
    
  }
  
  function JokeController($scope, $http) {
    $scope.displayJoke = '';
    $http.get('https://api.icndb.com/jokes/random')
      .then(function(result){
        console.log('joke: ', result);
        $scope.displayJoke = result.data.value.joke;
      });
  }
});

