angular.module('admin').directive('directiveFileManagement', function () {
  return {
    controller: FileManagementController,
    link: postLink,
    scope: {
      callback: '&',
      currentdoc: '=',
      currentdoctype: '='
    },
    templateUrl: 'directive/fileManagement/fileManagement/view.html'
  };
  
  function postLink(scope, iElement, iAttrs, vm) {
    
  }
  
  function FileManagementController($scope, $http, $rootScope, $q, $filter, Upload) {
    $scope.currentassociations = {};
    $scope.currentassociations[$scope.currentdoctype] = $scope.currentdoc._id;
  }
});

