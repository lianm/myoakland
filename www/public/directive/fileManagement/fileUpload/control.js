angular.module('admin').directive('directiveFileUpload', function () {
  return {
    controller: FileUploadController,
    link: postLink,
    scope: {
      callback: '&',
      currentassociations: '='
    },
    templateUrl: 'directive/fileManagement/fileUpload/view.html'
  };
  
  function postLink(scope, iElement, iAttrs, vm) {
    
  }
  
  function FileUploadController($scope, $http, $rootScope, $q, $filter, Upload) {
    console.log("fileUpload currentAssociations: ", $scope.currentassociations);
  
    $scope.uploadingFiles = 0;
    $scope.uploadItems = function (files, uploadType, uploadTypeId) {
      console.log("files: ", files);
      if (files && files.length) {
        for (var i = 0; i < files.length; i++) {
          $scope.uploadingFiles = $scope.uploadingFiles + 1;
          var file = files[i];
          if (!file.$error) {
            var toDirectory = 'upload/';
            Upload.upload({
              // url: '/uploadtest',
              url: '/api/files/fileUpload',
              data: {
                file: file,
                directory: toDirectory,
                uploadType: uploadType,
                uploadTypeId: uploadTypeId,
                currentAssociations: $scope.currentassociations
              }
            }).then(function (resp) {
              console.log("resp: ", resp);
              $scope.uploadingFiles = $scope.uploadingFiles - 1;
              $rootScope.refreshDirective = !$rootScope.refreshDirective;
              
            }, function (resp) {
              console.log('Error status: ', resp);
            }, function (evt) {
              console.log('evt: ', evt);
            });
          }
        }
      }
    
    };
    
  }
});

