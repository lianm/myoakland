angular.module('admin').directive('directiveFileList', function () {
  return {
    controller: FileListController,
    link: postLink,
    scope: {
      callback: '&',
      currentassociations: '='
    },
    templateUrl: 'directive/fileManagement/fileList/view.html'
  };
  
  function postLink(scope, iElement, iAttrs, vm) {
    
  }
  
  function FileListController($scope, $http, $rootScope, $q, $filter, Upload) {
    $scope.fileAssociations = [
      'user', 'pages', 'contentBlocks', 'business', 'project', 'item', 'colour', 'land', 'package'
    ]
    
    console.log("fileList currentAssociations: ", $scope.currentassociations);
    
    $rootScope.$watch('refreshDirective', function(newVal, oldVal) {
      console.log("refreshDirective: ", newVal);
      getMediaFiles();
    })
    
    $scope.fileQuery = {};
    
    $scope.mediaFiles = [];
    function getMediaFiles() {
      if ($scope.currentassociations) {
        $scope.fileQuery = $scope.currentassociations;
      }
      $http.post("/api/files/findall", $scope.fileQuery)
        .then(function successCallback(result){
          $scope.mediaFiles = result.data;
          console.log("$scope.mediaFiles: ", $scope.mediaFiles);
        }, function errorCallback(response) {
          console.log('error mediaFiles');
        });
    }
    getMediaFiles();
  
    $scope.updateMediaFile = function(thisFile) {
      $http.post("/api/files/update", thisFile)
        .then(function successCallback(result){
          console.log('updated mediaFile: ', result);
          getMediaFiles()
        }, function errorCallback(response) {
          console.log('error updating mediaFiles');
        });
    }
  
    $scope.fileTypes = [];
    function getFileTypes() {
      $http.get('/api/fileTypes/findall')
        .then(function(result){
          $scope.fileTypes = result.data;
        });
    }
    getFileTypes();
  
    $scope.archiveMediaFile = function(thisFile) {
      var updateThis = thisFile;
      updateThis.archive = !updateThis.archive;
      $scope.updateMediaFile(updateThis);
    }
    
    $scope.hideThis = "archive";
    $scope.showhide = function(useThis) {
      if ($scope.hideThis === useThis) {
        $scope.hideThis = "";
      } else {
        $scope.hideThis = useThis;
      }
    }
  }
});

