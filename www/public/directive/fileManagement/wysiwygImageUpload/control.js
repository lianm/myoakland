

angular.module('admin').config(function ($provide) {
  
  $provide.decorator('taOptions', ['taRegisterTool', '$delegate', '$uibModal', function (taRegisterTool, taOptions, $uibModal) {
    taRegisterTool('uploadImage', {
      buttontext: 'Upload Image',
      iconclass: "fa fa-image",
      action: function (deferred,restoreSelection) {
        $uibModal.open({
          controller: 'UploadImageModalInstance',
          templateUrl: 'directive/fileManagement/wysiwygImageUpload/view.html',
          backdropClass: 'wysiwygModalBackdrop',
          windowTopClass: 'wysiwygModal'
        }).result.then(
          function (result) {
            console.log("insert result", result);
            var addThis = "<p><img class='img-responsive' src='" + 'http://' + result.metaData.bucket + '/' + result.storeName + "'/></p>";
            restoreSelection();
            // document.execCommand('insertImage', true, result);
            document.execCommand('insertHTML', true, addThis);
            deferred.resolve();
          },
          function () {
            deferred.resolve();
          }
        );
        return false;
      }
    });
    taOptions.toolbar[3].push('uploadImage');
    return taOptions;
  }]);
});


angular.module('admin').controller('UploadImageModalInstance', function($scope, $uibModalInstance, Upload, $http, $rootScope){
  
  $scope.selectedImage = {};
  
  $scope.selectImage = function(thisImage) {
    if ($scope.selectedImage._id === thisImage._id) {
      $scope.insert();
    } else {
      $scope.selectedImage = thisImage;
    }
  };
  
  $scope.mediaFiles = [];
  function getMediaFiles() {
    if ($scope.currentassociations) {
      $scope.fileQuery = $scope.currentassociations;
    }
    $http.post("/api/files/findall", $scope.fileQuery)
      .then(function successCallback(result){
        $scope.mediaFiles = result.data;
        console.log("$scope.mediaFiles: ", $scope.mediaFiles);
      }, function errorCallback(response) {
        console.log('error mediaFiles');
      });
  }
  getMediaFiles();
  
  $scope.insert = function(){
    $uibModalInstance.close($scope.selectedImage);
  };
  
  $scope.showImageUploader = false;
  $scope.imageUploader = function(event) {
    event.preventDefault();
    console.log("showImageUploader");
    $scope.showImageUploader = !$scope.showImageUploader;
  }
  
  $rootScope.$watch('refreshDirective', function(newVal, oldVal) {
    console.log("refreshDirective: ", newVal);
    $scope.showImageUploader = false;
    getMediaFiles();
  })
})
