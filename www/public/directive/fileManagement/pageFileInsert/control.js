angular.module('admin').directive('directivePageFileInsert', function () {
  return {
    controller: FileListController,
    link: postLink,
    scope: {
      callback: '&',
      currentpage: '=',
      currentpagetype: '='
    },
    templateUrl: 'directive/fileManagement/pageFileInsert/view.html'
  };
  
  function postLink(scope, iElement, iAttrs, vm) {
    
  }
  
  function FileListController($scope, $http, $rootScope, $q, $filter, Upload) {
    console.log(".....FileListController.....")
    
    $scope.mediaFiles = [];
    $scope.getMediaFiles = function() {
      $http.post("/api/files/findall", {})
        .then(function successCallback(result){
          $scope.mediaFiles = result.data;
          console.log("$scope.mediaFiles: ", $scope.mediaFiles);
        }, function errorCallback(response) {
          console.log('error mediaFiles');
        });
    }
  
    $scope.fileInsertModalOpen = false;
    $scope.toggleFileInsertModalOpen = function() {
      $scope.fileInsertModalOpen = !$scope.fileInsertModalOpen;
    }
    
  }
});

