angular.module('admin').directive('directivePageFileList', function () {
  return {
    controller: FileListController,
    link: postLink,
    scope: {
      callback: '&',
      currentdoc: '='
    },
    templateUrl: 'directive/fileManagement/pageFileList/view.html'
  };
  
  function postLink(scope, iElement, iAttrs, vm) {
    
  }
  
  function FileListController($scope, $http, $rootScope, $q, $filter, Upload) {
    console.log("directivePageFileList $scope.currentdoc: ", $scope.currentdoc);
    
    $scope.fileTypes = [];
    function getFileTypes() {
      $http.get('/api/fileTypes/findall')
        .then(function(result){
          $scope.fileTypes = result.data;
        });
    }
    getFileTypes();
    
    
  }
});

