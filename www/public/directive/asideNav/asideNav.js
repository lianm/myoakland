angular.module('admin')
    .directive('asideNav',function () {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                name: "=",
                icon: "=",
                items: "=",
                state: "=",
                badgeClass: "=",
                badgeValue: "="
            },
            templateUrl: 'directive/asideNav/asideNav.html',
            link: function (scope, element, attrs, fn) {
                scope.data = [];
            },
            controller: function($scope, $state, $rootScope, $http){
                var allTypes = [
                    "Print Magazines",
                    "Newspapers",
                    "Major Publications",
                    "Direct Mail",
                    "Letterbox Delivery",
                    "Digital",
                    "Radio",
                    "SEO",
                    "Public Relations"
                ];
                $scope.productTypes = allTypes;

                $scope.search = {
                    productTypes: [
                        "Print Magazines",
                        "SEO"
                    ]
                }

                $scope.addType = function(type) {
                    $scope.search.productTypes.push(type);
                }
                $scope.removeType = function(index) {
                    $scope.search.productTypes.splice(index, 1);
                    $scope.productTypes = allTypes;
                }
            }
        };
    });

