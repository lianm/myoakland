angular.module('admin').directive('footer', function () {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            app: "="
        },
        templateUrl: 'directive/footer/footer.html',
        link: function (scope, element, attrs, fn) {

        }
    };
});

