angular.module('admin').directive('toolbar', function () {
  return {
    restrict: 'E',
    replace: true,
    scope: {
      app: "="
    },
    templateUrl: 'directive/toolbar/toolbar.html',
    controller: function($scope, authentication, $state, $http){
      $scope.logOut = function(){
        $http.get("/api/user/logout")
          .then(function(result){
            console.log("logout result: ", result);
          });
        
        authentication.currentUser = '';
        $state.go('signIn');
      };
    }
  };
});

