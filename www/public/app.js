angular.module('admin', [
  'ui.bootstrap',
  'ui.router',
  'ui.router.state.events',
  'ngAnimate',
  'ngTouch',
  'ngStorage',
  'textAngular',
  'ngAutocomplete',
  'ngFileUpload',
  'angularMoment']);

angular.module('admin').constant('clientTokenPath', '/path-or-url-to-your-client-token');

angular.module('admin').config(function($stateProvider, $urlRouterProvider, $httpProvider, $logProvider) {
  $logProvider.debugEnabled(true);
  
  $stateProvider
    .state('app', {
      abstract: true,
      url: '/app',
      templateUrl: 'partial/App/App.html'
    });
  
  $stateProvider.state('app.home', {
    url: '/home',
    templateUrl: 'partial/home/home.html',
    data: {
      requireLogin: true
    }
  });
  $stateProvider.state('app.personalProfile', {
    url: '/personalProfile',
    templateUrl: 'partial/personalProfile/personalProfile.html',
    data: {
      requireLogin: true
    }
  });
  $stateProvider.state('signIn', {
    url: '/signIn',
    templateUrl: 'partial/signIn/signIn.html',
    data: {
      requireLogin: false
    }
  });
  $stateProvider.state('app.adminUsers', {
    url: '/admin/users',
    templateUrl: 'partial/adminUsers/users.html',
    data: {
      requireLogin: true
    }
  });
  
  $stateProvider.state('app.adminMenus', {
    url: '/admin/menus',
    templateUrl: 'partial/adminMenus/view.html',
    data: {
      requireLogin: true
    }
  });
  
  
  $stateProvider.state('app.adminLocationPoints', {
    url: '/admin/locationPoints',
    templateUrl: 'partial/adminLocationPoints/view.html',
    data: {
      requireLogin: true
    }
  });
  $stateProvider.state('app.adminLocationCategorys', {
    url: '/admin/locationCategories',
    templateUrl: 'partial/adminLocationCategory/view.html',
    data: {
      requireLogin: true
    }
  });
  
  $stateProvider.state('app.adminHomepage', {
    url: '/admin/homepage',
    templateUrl: 'partial/adminHomepage/view.html',
    data: {
      requireLogin: true
    }
  });
  
  
  /**
   * ADMIN PAGES AND PAGE IDS
   */
  $stateProvider.state('app.adminPages', {
    url: '/pages',
    templateUrl: 'partial/adminPages/listPages/view.html',
    data: {
      requireLogin: true
    }
  });
  $stateProvider.state('app.adminPageId', {
    url: '/page/:pageId',
    templateUrl: 'partial/adminPages/appendPage/view.html',
    data: {
      requireLogin: true
    }
  });
  $stateProvider.state('app.adminNewPageId', {
    url: '/page/new',
    templateUrl: 'partial/adminPages/appendPage/view.html',
    data: {
      requireLogin: true
    }
  });
  /**
   * END - ADMIN PAGES AND PAGE IDS
   */
  
  
  /**
   * ADMIN ENQUIRY FORM
   */
  
  $stateProvider.state('app.adminContactForm', {
    url: '/enquiries',
    templateUrl: 'partial/adminContactForm/listContactForms/view.html',
    data: {
      requireLogin: true
    }
  });
  
  $stateProvider.state('app.adminContactFormId', {
    url: '/enquiry/:enquiryId',
    templateUrl: 'partial/adminContactForm/appendContactForm/view.html',
    data: {
      requireLogin: true
    }
  });
  $stateProvider.state('app.adminContactFormNew', {
    url: '/enquiry/new',
    templateUrl: 'partial/adminContactForm/appendContactForm/view.html',
    data: {
      requireLogin: true
    }
  });
  
  /**
   * END - ADMIN ENQUIRY FORM
   */
  
  
  
  /**
   * ADMIN CONTENT BLOCKS
   */
  
  $stateProvider.state('app.adminContentBlocks', {
    url: '/contentBlocks',
    templateUrl: 'partial/adminContentBlocks/listContentBlocks/view.html',
    data: {
      requireLogin: true
    }
  });
  
  $stateProvider.state('app.adminContentBlockId', {
    url: '/contentBlock/:blockId',
    templateUrl: 'partial/adminContentBlocks/appendContentBlock/view.html',
    data: {
      requireLogin: true
    }
  });
  $stateProvider.state('app.adminNewContentBlock', {
    url: '/contentBlock/new',
    templateUrl: 'partial/adminContentBlocks/appendContentBlock/view.html',
    data: {
      requireLogin: true
    }
  });
  
  // CREATE & EDIT CONTENT BLOCK TYPES
  $stateProvider.state('app.adminContentBlockTypes', {
    url: '/contentBlocksTypes',
    templateUrl: 'partial/adminContentBlockTypes/view.html',
    data: {
      requireLogin: true
    }
  });
  
  /**
   * END - ADMIN CONTENT BLOCKS
   */
  
  
  
  
  
  /**
   * ADMIN ESTATE MANAGEMENT > LAND
   */
  
  $stateProvider.state('app.adminLand', {
    url: '/lands',
    templateUrl: 'partial/adminEstateManagement/Land/listLand/view.html',
    data: {
      requireLogin: true
    }
  });
  $stateProvider.state('app.adminLandId', {
    url: '/land/:lotId',
    templateUrl: 'partial/adminEstateManagement/Land/appendLand/view.html',
    data: {
      requireLogin: true
    }
  });
  $stateProvider.state('app.adminNewLand', {
    url: '/land/new',
    templateUrl: 'partial/adminEstateManagement/Land/appendLand/view.html',
    data: {
      requireLogin: true
    }
  });
  /**
   * END - ADMIN ESTATE MANAGEMENT > LAND
   */
  
  
  
  /**
   * - ADMIN ESTATE MANAGEMENT > PACKAGES
   */
  $stateProvider.state('app.adminPackages', {
    url: '/packages',
    templateUrl: 'partial/adminEstateManagement/Packages/listPackages/view.html',
    data: {
      requireLogin: true
    }
  });
  $stateProvider.state('app.adminPackageId', {
    url: '/package/:packageId',
    templateUrl: 'partial/adminEstateManagement/Packages/appendPackages/view.html',
    data: {
      requireLogin: true
    }
  });
  $stateProvider.state('app.adminNewPackage', {
    url: '/package/new',
    templateUrl: 'partial/adminEstateManagement/Packages/appendPackages/view.html',
    data: {
      requireLogin: true
    }
  });
  /**
   * END - ADMIN ESTATE MANAGEMENT > LAND
   */
  
  
  /**
   * - ADMIN ESTATE MANAGEMENT > HOMES
   */
  $stateProvider.state('app.adminHomes', {
    url: '/homes',
    templateUrl: 'partial/adminEstateManagement/Homes/listHomes/view.html',
    data: {
      requireLogin: true
    }
  });
  $stateProvider.state('app.adminHomeId', {
    url: '/homes/:homeId',
    templateUrl: 'partial/adminEstateManagement/Homes/appendHomes/view.html',
    data: {
      requireLogin: true
    }
  });
  $stateProvider.state('app.adminNewHome', {
    url: '/homes/new',
    templateUrl: 'partial/adminEstateManagement/Homes/appendHomes/view.html',
    data: {
      requireLogin: true
    }
  });
  /**
   * END - ADMIN ESTATE MANAGEMENT > HOMES
   */
  
  
  /**
   * - ADMIN ESTATE MANAGEMENT > BUILDERS
   */
  
  $stateProvider.state('app.adminBuilders', {
    url: '/builders',
    templateUrl: 'partial/adminEstateManagement/Builders/listBuilders/view.html',
    data: {
      requireLogin: true
    }
  });
  $stateProvider.state('app.adminBuilderId', {
    url: '/builder/:builderId',
    templateUrl: 'partial/adminEstateManagement/Builders/appendBuilders/view.html',
    data: {
      requireLogin: true
    }
  });
  $stateProvider.state('app.adminNewBuilder', {
    url: '/builder/new',
    templateUrl: 'partial/adminEstateManagement/Builders/appendBuilders/view.html',
    data: {
      requireLogin: true
    }
  });
  
  /**
   * END - ADMIN ESTATE MANAGEMENT > BUILDERS
   */
  
  
  
  $stateProvider.state('app.adminFileManagement', {
    url: '/file-management',
    templateUrl: 'partial/fileManagement/adminFileManagement/view.html',
    data: {
      requireLogin: true
    }
  });
  $stateProvider.state('app.adminFileTypes', {
    url: '/file-types',
    templateUrl: 'partial/fileManagement/adminFileTypes/view.html',
    data: {
      requireLogin: true
    }
  });
  
  
    
    /* Add New States Above */
  $urlRouterProvider.otherwise('/signIn');
  
  $httpProvider.interceptors.push(function() {
    return {
      'responseError': function(rejection) {
        if(rejection.status === 401){
          window.location.replace('#/signIn');
        } else {
          console.log("Error", rejection);
          // if(rejection.data.message) alert(rejection.data.message);
        }
      }
    };
  });
  
});


angular.module('admin').run(function($rootScope, $templateCache, $http, authentication) {
  $rootScope.safeApply = function(fn) {
    var phase = $rootScope.$$phase;
    if (phase === '$apply' || phase === '$digest') {
      if (fn && (typeof(fn) === 'function')) {
        
        fn();
      }
    } else {
      this.$apply(fn);
    }
  };
});







