
$(document).ready(function(){
  $(function(){
  $('#searchFields').submit(function(e){
    e.preventDefault();

    var form = $(this);
    var formData = JSON.parse(JSON.stringify(form.serializeArray()));

      console.log("package search: ", formData);

    $('#loader3', form).html('<img src="../../images/ajax-loader.gif" />       Please wait...');

    var urlData = form.serialize();
      console.log("urlData: ", urlData);
    var newUrl = window.location.href.split('?')[0] + '?' + urlData;

    $.ajax({
      type: 'GET',
      url: '/i/ajaxLand',
      data: formData,
      success: function(ajaxResults) {
        console.log("ajaxResults: ", ajaxResults);
        $('#resultsContent').html(ajaxResults);
        window.history.pushState({"pageTitle": 'Somerfield Estate, Holmview'}, "", newUrl);
        }
      });
    });
  });
});


var gridHidden = false;
function changeDisplay(e) {
  e.preventDefault();
  gridHidden = !gridHidden;
  hideItems();
}

var soldHidden = true;
function changeSold(e) {
  e.preventDefault();
  soldHidden = !soldHidden;
  hideItems();
}

var holdHidden = true;
function changeHold(e) {
  e.preventDefault();
  holdHidden = !holdHidden;
  hideItems();
}


function hideItems() {
  hideContainer = '#resultsDiv';
  showContainer = '#resultsGrid';
  if (gridHidden) {
    var hideContainer = '#resultsGrid';
    var showContainer = '#resultsDiv';
  }
  $(hideContainer).show();
  $(showContainer).hide();

  if (soldHidden) {
    $(hideContainer + ' .landSold').hide();
  } else {
    $(hideContainer + ' .landSold').show();
  }

  if (holdHidden) {
    $(hideContainer + ' .landHold').hide();
  } else {
    $(hideContainer + ' .landHold').show();
  }
}





function landContent() {
  console.log("at the start!");
  var resultsDivContent = '<div id="resultsDiv">';
  var resultsGridContent = '<div id="resultsGrid">';

  resultsGridContent += '<div class="flexGrid gridHeading">';
  resultsGridContent += '<div class="flexCol">Lot</div>';
  resultsGridContent += '<div class="flexCol">Stage</div>';
  resultsGridContent += '<div class="flexCol">Size</div>';
  resultsGridContent += '<div class="flexCol">Frontage</div>';
  resultsGridContent += '<div class="flexCol">Depth</div>';
  resultsGridContent += '<div class="flexCol">Price</div>';
  resultsGridContent += '<div class="flexCol">Status</div>';
  resultsGridContent += '<div class="flexCol">Documents</div>';
  resultsGridContent += '</div>';

  landResults.forEach(function(landResult) {

    /**
     * DIV AREA
     */
    resultsDivContent += '<div class="landChild ';
    if (landResult.sold) {
      resultsDivContent += 'landSold';
    } else if (landResult.hold) {
      resultsDivContent += 'landHold';
    }
    resultsDivContent += '">';

    if (landResult.sold) {
      resultsDivContent += '<div class="ribbon soldRibbon">Sold</div>';
    } else if (landResult.hold) {
      resultsDivContent += '<div class="ribbon holdRibbon">Hold</div>';
    }

    resultsDivContent += '<div><img class="img-responsive" src="/assets/images/somerfield/land/images/Lot' + landResult.lotNumber + '.png" /></div>';


    resultsDivContent += '<div style="padding:10px; padding-top: 0;">';

    resultsDivContent += '<div>';
    resultsDivContent += '<a href="/i/lotDetails/' + landResult._id + '">' + 'Lot ' + landResult.lotNumber + ' </a>';
    resultsDivContent += '<span class="small">(Stage ' + landResult.stage + ')</span>';
    resultsDivContent += '</div>';

    resultsDivContent += '<h2 style="margin-top:0; margin-bottom:0;" class="landPriceShow">$';
    if (landResult.sold) {
      resultsDivContent += ' SOLD';
    } else {
      resultsDivContent += landResult.price.toLocaleString('en');
    }
    resultsDivContent += '</h2>';


    resultsDivContent += '<div class="small">';
    resultsDivContent += '<span>' + landResult.size + 'm2<span>';
    resultsDivContent += '<span> (' + landResult.frontage + 'm x ' + landResult.depth + 'm)<span>';
    resultsDivContent += '</div>';

    resultsDivContent += '<div>';
    resultsDivContent += '<a class="small" href="/assets/images/somerfield/land/disclosures/Lot' + landResult.lotNumber + '.pdf" target="_blank">Lot Disclosure</a>';
    resultsDivContent += '</div>';

    resultsDivContent += '</div>';


    resultsDivContent += '</div>';

    /**
     * GRID AREA
     */
    resultsGridContent += '<div class="flexGrid ';
    if (landResult.sold) {
      resultsGridContent += 'landSold';
    } else if (landResult.hold) {
      resultsGridContent += 'landHold';
    }
    resultsGridContent += '">';

    resultsGridContent += '<div class="flexCol">' + landResult.lotNumber + '</div>';
    resultsGridContent += '<div class="flexCol">' + landResult.stage + '</div>';
    resultsGridContent += '<div class="flexCol">' + landResult.size + 'm2</div>';
    resultsGridContent += '<div class="flexCol">' + landResult.frontage + 'm</div>';
    resultsGridContent += '<div class="flexCol">' + landResult.depth + 'm</div>';
    resultsGridContent += '<div class="flexCol">$';
    if (landResult.sold) {
      resultsGridContent += ' SOLD';
    } else {
      resultsGridContent += landResult.price.toLocaleString('en');
    }
    resultsGridContent += '</div>';

    resultsGridContent += '<div class="flexCol">';
    if (landResult.sold) {
      resultsGridContent += 'Sold';
    } else if (landResult.hold) {
      resultsGridContent += 'Hold';
    } else {
      resultsGridContent += 'Available';
    }
    resultsGridContent += '</div>';

    resultsGridContent += '<div class="flexCol">';
    resultsGridContent += '<a class="small" href="/assets/images/land/disclosures/oakland-disclosure-lot' + landResult.lotNumber + '.pdf" target="_blank">Lot Disclosure</a>';
    resultsGridContent += '</div>';

    resultsGridContent += '</div>';
  });
  resultsDivContent += '</div>';
  resultsGridContent += '</div>';

  console.log("at the end!");
  $('#resultsContent').html(resultsDivContent + resultsGridContent);
  hideItems();
}
