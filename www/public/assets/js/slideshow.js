// Really Simple Slideshow by Jorge Viveros
// Version 1.0
//sixtwoone.design@gmail.com


$(document).ready(function(){
    var i, j = 0;
    var slides = $("#panel").children().size();
    var slidePause = 3000;
    var slideshowDuration = (slides * slidePause) + 500;
    var slideWidth = parseInt($(".slide").css("width"));
    var slideDistance = "-=" + slideWidth + "px";
    var resetDistance = "+=" + (slideWidth * (slides-1)) + "px";
    var resetCircles;
    var innerWidth;
    var circleWidth;
    var w = slides * slideWidth;
    var panelWidth = w + "px";
    var circleDistance;


    $("#panel").css("width", panelWidth);//set slide container width

    $("#display").append('<ul id = "inner-box"><li class = "selected"></li></ul>');//add display children

    for(j=0;j<=slides-1;j++){//begin for

        $("#display").append('<li class = "circle"></li>');

    };//end for

    circleWidth = parseInt($(".circle").css("width")) + parseInt($(".circle").css("margin-left")); //get total width of circle (width + margin)

    innerWidth = (slides * circleWidth) + "px";
    $("#inner-box").css("width", innerWidth);//set inner box width

    circleDistance = "+=" + circleWidth + "px";//set distance circle will travel
    resetCircles = "-=" + (circleWidth*(slides-1)) + "px";



    play(); //call play function once

    setInterval(function() {//call Play function every X seconds
        play();				// where X = "slideshowDuration"
    }, slideshowDuration);




    function play(){  //define Play function

        for(i=0; i<=slides-2; i++){
            $("#panel").delay(slidePause).animate({"left" : slideDistance}, 600,"swing",//change slide every x seconds.  x = slideDistance.

                function(){//begin callback

                    $(".selected").fadeOut(100);
                    $(".selected").animate({"left" : circleDistance},50);
                    $(".selected").fadeIn(100);

                }//end callback
            );//end slide

            //Check if this is the last slide.  If so, reset slides

            if(i == slides-2){
                $("#panel").delay(slidePause).fadeOut(100);
                $("#panel").animate({"left" : resetDistance}, 50);
                $("#panel").fadeIn(100,

                    function(){//begin callback

                        $(".selected").fadeOut(100);
                        $(".selected").animate({"left" : resetCircles},50);
                        $(".selected").fadeIn(100);

                    }//end callback

                );//end fadeIn
            }//end if
        };//end for

    };//end play

});
