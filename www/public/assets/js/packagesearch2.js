$(document).ready(function(){
    $(function(){
    $('#packageSearch').submit(function(e){
      e.preventDefault();

      var form = $(this);
      var formData = JSON.parse(JSON.stringify(form.serializeArray()));

        console.log("package search: ", formData);

      $('#loader3', form).html('<img src="../../images/ajax-loader.gif" />       Please wait...');

      var urlData = form.serialize();
      console.log("urlData: ", urlData);
      var newUrl = window.location.href.split('?')[0] + '?' + urlData;

      $.ajax({
        type: 'GET',
        url: '/i/ajaxHousePackages',
        data: formData,
        success: function(ajaxResults) {
          console.log("ajaxResults: ", ajaxResults);
          $('#resultsContent').html(ajaxResults);
          window.history.pushState({"pageTitle":'Somerfield Estate, Holmview'},"", newUrl);
        }
      });
    });
  });
});

function useReturnData(data){
    myvar = data;
    console.log(myvar);
};

$('.builder').on('click', function(){
    if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $(this).find('input').prop('checked',false);
    } else {
        $(this).addClass('active');
        $(this).find('input').prop('checked',true);
    }

    // $(this).parent().addClass("thisParent")

    return false;
});


$(document).scroll(function() {
  var searchAttributes = $('#packageSearch');
  var div = $('.packageMenu');
  var searchAttributesHeight = searchAttributes.outerHeight();
  var divHeight = div.height();
  var searchAttributesOffset = searchAttributes.offset().top + searchAttributesHeight;
  var divOffset = div.offset().top + divHeight;

  if (searchAttributesOffset >= divOffset) {
    searchAttributes.addClass('hitbottom');

    var windowScroll = $(window).scrollTop() + $(window).height();
    if (searchAttributesOffset > windowScroll) {
      searchAttributes.removeClass('hitbottom');
    }
  }
});



$(".packageMenuButton").click(function() {
    $('body').toggleClass('packageMenuTrue');
    $('.packageMenu').slideToggle(function(){
        var i = 0;
        $(this).animate({height: (++i % 2) ? ($(window).height() - 100) : 80}, 200);
    });
});

$(".landChild").click(function() {
    $('.information').slideToggle(function(){
    });
});


$('a.filter-item').on('click', function(){
  if ($(this).hasClass('active')) {
    $(this).removeClass('active');
    $(this).find('input').prop('checked',false);
  } else {
    $(this).addClass('active');
    $(this).find('input').prop('checked',true);
  }
  return false;
});
