
 * http://appic.designzway.com/

$(document).ready(function() {
    var carousel = $('.carousel');
    if (carousel) {
        $('.carousel').carousel({
            interval: 2000
        });
    }
    
    
    /**
     * MENU SCROLL CHANGE ACTIVE
     var menu = $('#menuContact');
     $(window).scroll(function () {
    var h = $(window).height();
    var y = $(this).scrollTop();
    var z = $('#ContactUs').offset().top;

    if (h + y >= z + h - 120) {
      menu.removeClass('inactive').addClass('active');
    } else {
      menu.removeClass('active').addClass('inactive');
    }
  });
     
     
     var sections = ["#dropdownMenu1", "#dropdownMenu2"];
     
     // (type="button", data-toggle="dropdown", aria-haspopup="true", aria-expanded="true")
     var bindAttributes = [
     {
       name: 'type',
       value: 'button'
     },
     {
       name: 'data-toggle',
       value: 'dropdown'
     },
     {
       name: 'aria-haspopup',
       value: 'true'
     },
     {
       name: 'aria-expanded',
       value: 'true'
     }
     ];
     **/
    function addMobileAttributes() {
        for (i = 0; i < sections.length; i++) {
            var section = $(sections[i]);
            console.log("section: ", section);
            for (b = 0; b < bindAttributes.length; b++) {
                section.attr(bindAttributes[b].name, bindAttributes[b].value);
            }
        }
        
        $('.viewSearch').show();
        $('.showExtraSearchView').hide();
    }
    
    function removeMobileAttributes() {
        for (i = 0; i < sections.length; i++) {
            var section = $(sections[i]);
            for (b = 0; b < bindAttributes.length; b++) {
                section.removeAttr(bindAttributes[b].name, bindAttributes[b].value);
            }
        }
        
        $('.viewSearch').hide();
        $('.showExtraSearchView').show();
    }
    
    function checkMobile() {
        var width = $(window).width();
        if (width < 768) {
            $('body').addClass("mobile");
            addMobileAttributes();
            console.log("Add Mobile");
        } else {
            $('body').removeClass("mobile");
            removeMobileAttributes();
            console.log("Remove Mobile");
        }
    }
    
    checkMobile();
    
    $(window).resize(function () {
        checkMobile();
    });
    
    
    $('.dropdown-menu').click(function () {
        console.log('Submenu Clicked');
        var width = $(window).width();
        if (width < 768) {
            $('.navbar-toggle').click();
        }
    });
    
    hideItems();
});
