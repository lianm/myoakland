
var packageResults = [];

function changeViewSearch(e) {
  e.preventDefault();
  console.log("test this");
  $('.showExtraSearchView').toggle();
}
function currencyFormat(onumber, n, x, s, c) {
    /**
     * @param integer n: number of decimal places
     * @param integer x: length of grouped numbers
     * @param mixed   s: grouped number delimiter
     * @param mixed   c: decimal delimiter
     */

    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = onumber.toFixed(Math.max(0, ~~n));

    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
}


$(document).ready(function(){
    $(function(){
    $('#packageSearch').submit(function(e){
      e.preventDefault();

      var form = $(this);
      var formData = JSON.parse(JSON.stringify(form.serializeArray()));

      console.log("package search: ", formData);

      $('#loader3', form).html('<img src="../../images/ajax-loader.gif" />       Please wait...');

      var urlData = form.serialize();
      var newUrl = window.location.href.split('?')[0] + '?' + urlData;

      $.ajax({
        type: 'POST',
        url: '/api/packages/packageSearch2',
        data: formData,
        success: function(formResults) {
          packageResults = formResults;
          console.log("packageResults: ", packageResults);
          if (packageResults.packageDetails.length > 0) {
            packageContent();
          } else {
            var noResults = 'No properties available within your current search criteria. ';
            noResults += '<a href="/i/home-land">Clear Search</a>';
            $('#resultsContent').html(noResults);
          }
          window.history.pushState({"pageTitle":'Test'},"", newUrl);
        }
      });
    });
  });
});



function packageContent() {
    var resultsDivContent = '<div id="resultsDiv">';
    resultsDivContent += '<div class="landParent">';

    var imageBaseURL = 'http://clientstorage.altitudepm.com.au/';


    packageResults.packageDetails.forEach(function (packageResult) {

        /**
         resultsDivContent += '<div class="landChild">';
         resultsDivContent += '<div class="packageImage">';
         http://clientstorage.altitudepm.com.au/" + fileDetails.allFoundFiles[fileDetails.fileCategory.package[packageResult._id]['59c4903c6fbf80ee889d2039'].files[0]].storeName
         resultsDivContent +=  '<img class="img-responsive" style="width: 100%"' />
         resultsDivContent += '</div>';
         */

        resultsDivContent += '<div class="landChild">';
        resultsDivContent += '<div class="packageImage">';
        resultsDivContent += '    <img class="img-responsive" style="width: 100%;" src="' + imageBaseURL + packageResults.imageDetails.allFoundFiles[packageResults.imageDetails.fileCategory.package[packageResult._id]['59c4903c6fbf80ee889d2039'].files[0]].storeName + '">';
        resultsDivContent += '      </div>';
        resultsDivContent += '          <div class="packageDetails">';
        resultsDivContent += '            <div class="packageName">' + packageResult.houseName + '</div><div class="builderDetails"><span>by</span>';
        resultsDivContent += '        <a class="builderName" href="/i/builder/59c48ca4817eb6ee273b7064">' + packageResult.builder.name + '</a></div>';
        resultsDivContent += '<div class="packageIcons row text-center">';
        resultsDivContent += '  <div class="col-sm-4">';
        resultsDivContent += '     <div class="packageBedrooms">';
        resultsDivContent += '         <span class="icon-bg"><i class="fonticon-bed"></i></span>';
        resultsDivContent += '         <span>' + packageResult.bedrooms + '</span><span class="hidden-md hidden-lg"> Bedrooms</span>';
        resultsDivContent += '      </div>';
        resultsDivContent += '    </div>';
        resultsDivContent += '<div class="col-sm-4">';
        resultsDivContent += '  <div class="packageBathrooms">';
        resultsDivContent += '    <span class="icon-bg"><i class="fonticon-shower"></i></span>';
        resultsDivContent += '    <span>' + packageResult.bathrooms + '</span><span class="hidden-md hidden-lg"> Bathrooms</span>';
        resultsDivContent += '    </div>';
        resultsDivContent += '</div>';
        resultsDivContent += '<div class="col-sm-4">';
        resultsDivContent += '  <div class="packageCars">';
        resultsDivContent += '    <span class="icon-bg"><i class="fonticon-garage"></i></span>';
        resultsDivContent += '    <span>' + packageResult.cars +'</span><span class="hidden-md hidden-lg"> Cars</span>';
        resultsDivContent += '    </div>';
        resultsDivContent += '</div>';
        resultsDivContent += '</div>';
        resultsDivContent += '<div class="packageLand row text-center">';
        resultsDivContent += '  <div class="col-sm-4">';
        resultsDivContent += '      <div class="lotText">Lot</div>';
        resultsDivContent += '          <div class="lotNumber">';
        resultsDivContent += '              <a href="/i/lotDetails/59eb18b4bb1795372534ba60">' + packageResult.land.lotNumber + '</a>';
        resultsDivContent += '          </div>';
        resultsDivContent += '  </div>';
        resultsDivContent += '    <div class="col-sm-4">';
        resultsDivContent += '      <div class="lotText">House</div>';
        resultsDivContent += '        <div class="lotNumber">' + packageResult.houseSize + 'm2</div>';
        resultsDivContent += '    </div>';
        resultsDivContent += '        <div class="col-sm-4">';
        resultsDivContent += '            <div class="lotText">Land</div>';
        resultsDivContent += '                <div class="lotNumber">' + packageResult.landSize + 'm2</div>';
        resultsDivContent += '            </div>';
        resultsDivContent += '        </div>';
        resultsDivContent += '            <div class="packagePrice">$' + currencyFormat(packageResult.packagePrice, '', 3, ',', '.') + '</div>';
        resultsDivContent += '         </div>';
        resultsDivContent += '            <div class="packageEnquiry text-center">';
        resultsDivContent += '                <div class="row">';
        resultsDivContent += '                    <div class="col-xs-6"><i class="fonticon-download"></i>';
        resultsDivContent += '                        <div class="packageBrochure">'
        resultsDivContent += '          <a target="_blank" href="' + imageBaseURL + packageResults.imageDetails.allFoundFiles[packageResults.imageDetails.fileCategory.package[packageResult._id]['59c47ec81e66a3e5ca44a0ec'].files[0]].storeName + '">Brochure</a>';
        resultsDivContent += '                        </div>';
        resultsDivContent += '                    </div>';
        resultsDivContent += '                        <div class="col-xs-6">';
        resultsDivContent += '                             <a href="" onclick="return selectPackageEnquiry(\'' + packageResult._id + '\', \'' + packageResult.houseName + '\', \'' + packageResult.builder.name + '\',\'' + packageResult.land.lotNumber + '\')">';
        resultsDivContent += '                                <i class="fonticon-mail"></i>';
        resultsDivContent += '                                    <div class="packageMakeEnquiry">Enquire</div>';
        resultsDivContent += '                            </a>';
        resultsDivContent += '                       </div>';
        resultsDivContent += '                    </div>';
        resultsDivContent += '                </div>';
        resultsDivContent += '            </div>';

        $('#resultsContent').html(resultsDivContent);
    })
};

