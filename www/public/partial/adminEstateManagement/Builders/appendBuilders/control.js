angular.module('admin').controller('buildersAppendCtrl', function($scope, $http, $state, $rootScope, $stateParams, $timeout) {
  
  $rootScope.$watch('refreshDirective', function(newVal, oldVal) {
    if (oldVal !== newVal) {
      getBuilder();
    }
  });
  
  $scope.builder = {};
  function getBuilder() {
    $http.post('/api/builders/findAll', {_id: $stateParams.builderId})
      .then(function(result){
        $scope.builder = result.data[0];
      });
  }
  if ($stateParams.builderId) {
    getBuilder();
  }
  
  
  $scope.save = function(data){
    $scope.displaySaving = true;
    $http.post("/api/builders/create", data)
      .then(function(result){
        console.log("saved builder: ", result);
        $scope.displaySaving = false;
        $scope.displaySaved = true;
        $timeout(function () {
          $scope.displaySaved = false;
        }, 6000);
        
        if ($stateParams.builderId) {
          $scope.builder = result.data;
        } else {
          $state.go('app.adminBuilderId', {builderId: result.data._id})
        }
      }, function(err){
        $scope.authError = (err.message || "Sorry, an error occurred. Please try again.");
      });
  };
  
  
  $scope.remove = function(data){
    var r = confirm("Are you sure you want to delete this item?");
    if (r == true) {
      $http.post("/api/builders/remove", data)
        .then(function (result) {
          console.log("removed builder: ", result);
          $state.go('app.adminBuilders')
        }, function (err) {
          $scope.authError = (err.message || "Sorry, an error occurred. Please try again.");
        });
    }
  };
  
  
  
  $scope.blockShiftUp = function(highlight) {
    var fromHere = $scope.builder.highlights.indexOf(highlight);
    $scope.builder.highlights.splice(fromHere, 1);
    $scope.builder.highlights.splice(fromHere - 1, 0, highlight);
  };
  $scope.blockShiftDown = function(highlight) {
    var fromHere = $scope.builder.highlights.indexOf(highlight);
    $scope.builder.highlights.splice(fromHere, 1);
    $scope.builder.highlights.splice(fromHere + 1, 0, highlight);
  };
  
  $scope.blockDelete = function(highlight) {
    var fromHere = $scope.builder.highlights.indexOf(highlight);
    $scope.builder.highlights.splice(fromHere, 1);
  }
  
  $scope.newHighlight = '';
  $scope.addHighlight = function() {
    if ($scope.newHighlight.length > 0) {
      $scope.builder.highlights.push($scope.newHighlight);
      $scope.newHighlight = '';
    }
  }
  
  
});
