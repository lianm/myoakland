angular.module('admin').controller('buildersListCtrl', function($scope, $http, $timeout) {
  
  $scope.buildersResults = [];
  function getBuilders() {
    $http.get('/api/builders/findall')
      .then(function (result) {
        $scope.buildersResults = result.data;
      }, function (err) {
        console.log("error: ", err);
      })
  }
  getBuilders();
  
});
