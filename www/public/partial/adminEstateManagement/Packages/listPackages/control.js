angular.module('admin').controller('adminListPackagesCtrl', function($scope, $http, $timeout){
  
  $scope.packageResults = [];
  function getPackages() {
    $http.get('/api/packages/findallPopulate')
      .then(function(result){
        $scope.packageResults = result.data;
        console.log("$scope.packageResults: ", $scope.packageResults);
      });
  }
  getPackages();
  
  $scope.Object = Object;
  $scope.selectedRows = {};
  $scope.selectRow = function(thisRow) {
    if ($scope.selectedRows[thisRow._id]) {
      delete $scope.selectedRows[thisRow._id];
    } else {
      $scope.selectedRows[thisRow._id] = thisRow;
    }
    console.log('$scope.selectedRows: ', $scope.selectedRows);
  }
  
  $scope.selectAllRows = function() {
    if ($scope.selectedRows.length == $scope.packageResults.length) {
      $scope.selectedRows = {};
    } else {
      angular.forEach($scope.packageResults, function(thisRow, key) {
        $scope.selectedRows[thisRow._id] = thisRow;
      })
    }
  }
  
  $scope.deleteRows = function() {
    $http.post("/api/packages/removeMany", {_id: {"$in": Object.keys($scope.selectedRows)}})
      .then(function (result) {
        console.log("deleteRows: ", result);
        getPackages()
      }, function (err) {
        console.log("error: ", err);
      })
  }
  
  $scope.remove = function(data){
    var r = confirm("*WARNING* Are you sure you want to delete this pacakge?");
    if (r == true) {
      $http.post("/api/packages/remove", data)
        .then(function (result) {
          console.log("removed content package: ", result);
          getPackages()
        }, function (err) {
          console.log("error: ", err);
        })
    }
  };
});
