angular.module('admin').controller('adminAppendPackagesCtrl', function($scope, $stateParams, $http, $timeout){
  
  $scope.page = {};
  function getPackages() {
    $http.post("/api/packages/findpackage", {_id: $stateParams.packageId})
      .then(function(result){
        console.log("getPackages: ", result);
        $scope.page = result.data[0];
      });
  }
  if ($stateParams.packageId) {
    getPackages();
  }
  
  
  /**
   * MANAGE LAND RESULTS
   */
  $scope.landResults = [];
  $scope.landResultsObject = {};
  function getLand() {
    $http.get('/api/land/findall')
      .then(function(result){
        $scope.landResults = result.data;
        console.log("$scope.landResults: ", $scope.landResults);
        
        angular.forEach($scope.landResults, function(landDetails, key) {
          $scope.landResultsObject[landDetails._id] = landDetails;
        })
      });
  }
  getLand();
  
  $scope.changeLand = function(landId) {
    $scope.page.stage = $scope.landResultsObject[landId].stage;
    $scope.page.lotNumber = $scope.landResultsObject[landId].lotNumber;
    $scope.page.landPrice = $scope.landResultsObject[landId].landPrice;
    $scope.page.landSize = $scope.landResultsObject[landId].landSize;
  
    $scope.resetPackagePrice();
  }
  
  /**
   * END - MANAGE LAND RESULTS
   */
  
  
  /**
   * MANAGE HOME RESULTS
   */
  $scope.homeResults = [];
  $scope.homeResultsObject = {};
  function getHomes() {
    $http.post('/api/homes/findallPopulate', {})
      .then(function(result){
        $scope.homeResults = result.data;
        console.log("$scope.homeResults: ", $scope.homeResults);
        
        angular.forEach($scope.homeResults, function(homeDetails, key) {
          $scope.homeResultsObject[homeDetails._id] = homeDetails;
        })
        
      });
  }
  getHomes();
  
  $scope.changeHome = function(homeId) {
    console.log("$scope.homeResultsObject[homeId]: ", $scope.homeResultsObject[homeId]);
    if ($scope.homeResultsObject[homeId].builder) {
      $scope.page.builder = $scope.homeResultsObject[homeId].builder._id;
      $scope.page.builderName = $scope.homeResultsObject[homeId].builder.name;
    }
    
    $scope.page.buildPrice = $scope.homeResultsObject[homeId].buildPrice;
    $scope.page.houseSize = $scope.homeResultsObject[homeId].houseSize;
    $scope.page.bedrooms = $scope.homeResultsObject[homeId].bedrooms;
    $scope.page.bathrooms = $scope.homeResultsObject[homeId].bathrooms;
    $scope.page.cars = $scope.homeResultsObject[homeId].cars;
    $scope.page.houseLevels = $scope.homeResultsObject[homeId].houseLevels;
  
    $scope.page.packageBuildPrice = $scope.homeResultsObject[homeId].buildPrice;
    $scope.resetPackagePrice();
  }
  
  
  /**
   * END - MANAGE HOME RESULTS
   */
  
  $scope.resetPackagePrice = function() {
    if (!$scope.page.landPrice) {
      $scope.page.landPrice = 0;
    }
    
    if (!$scope.page.packageBuildPrice) {
      $scope.page.packageBuildPrice = 0;
    }
    $scope.page.packagePrice = $scope.page.landPrice + +$scope.page.packageBuildPrice;
  }
  
  $scope.changePackagePrice = function() {
    $scope.page.packageBuildPrice = $scope.page.packagePrice - $scope.page.landPrice;
    $scope.resetPackagePrice();
  }
  
  $scope.$watchCollection('page', function(newValue, oldValue) {
    $scope.page.packageId = "Lot " + $scope.page.stage + "-" + $scope.page.lotNumber + " " +
      $scope.page.builderName + " - " + $scope.page.houseName + " - " + $scope.page.houseSize + "m2 - " +
      $scope.page.bedrooms + $scope.page.bathrooms + $scope.page.loungerooms + $scope.page.cars + " - " +
      "$" + $scope.page.packagePrice;
  })
  
  $scope.save = function(data){
    $http.post('/api/packages/create', data)
      .then(function(result){
        console.log("saved: ", result);
        if ($stateParams.packageId) {
          $scope.page = result.data;
        } else {
          $state.go('app.adminPackageId', {packageId: result.data._id})
        }
      }, function(err){
        $scope.authError = (err.message || "Sorry, an error occurred. Please try again.");
      });
  };
  
  
  $scope.remove = function(data){
    var r = confirm("*WARNING* Are you sure you want to delete this pacakge?");
    if (r == true) {
      $http.post("/api/packages/remove", data)
        .then(function (result) {
          console.log("removed content package: ", result);
          $state.go('app.adminPackages')
        }, function (err) {
          console.log("error: ", err);
        })
    }
  };
  
});
