angular.module('admin').controller('landAppendCtrl', function($scope, $rootScope, $http, $state, $stateParams, $timeout){
  
  $scope.page = {};
  function getLand() {
    $http.post("/api/land/findland", {_id: $stateParams.lotId})
      .then(function(result){
        console.log("getLand: ", result);
        $scope.page = result.data[0];
      });
  }
  if ($stateParams.lotId) {
    getLand();
  }
  
  
  $rootScope.$watch('refreshDirective', function(newVal, oldVal) {
    if (oldVal !== newVal) {
      getLand();
    }
  })
  
  $scope.save = function(data){
    $http.post('/api/land/create', data)
      .then(function(result){
        console.log("$scope.save: ", result);
        $scope.page = result.data;
      }, function(err){
        $scope.authError = (err.message || "Sorry, an error occurred. Please try again.");
      });
  };
  
  $scope.delete = function(data){
    var r = confirm("Are you sure you want to delete this item?");
    if (r == true) {
      $http.post('/api/land/remove', data)
        .then(function(result){
          $state.go('app.adminLand');
        }, function(err){
          $scope.authError = (err.message || "Sorry, an error occurred. Please try again.");
        });
    }
  };
  
});
