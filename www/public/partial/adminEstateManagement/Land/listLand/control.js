angular.module('admin').controller('landListCtrl', function($scope, $http, $timeout){
  
  $scope.landResults = [];
  $scope.landResultsObject = {};
  function getLand() {
    $http.get('/api/land/findall')
      .then(function(result){
        $scope.landResults = result.data;
        
        $scope.landResultsObject = {};
        if ($scope.landResults.length > 0) {
          angular.forEach($scope.landResults, function(land, key){
            $scope.landResultsObject[land.lotId] = land;
    
            if (key == $scope.landResults.length -1) {
              $scope.getGooglePricelist();
            }
          });
        } else {
          $scope.getGooglePricelist();
        }
        console.log("$scope.landResults: ", $scope.landResults);
      });
    
  }
  getLand();
  
  
  /**
   * IMPORT LAND INFORMATION FROM GOOGLE SPREADSHEET
   */

  var googleApiKey = 'AIzaSyCZfOhKtmHpEAmSJzORrlyuUePowXOPWnU';
  var pricelistId = '1ph825t4OJXE2O4NH0ITczE17KiBQ0UGoaYyLzldi-5Y';
  var priceSheetName = 'GooglePricelist';
  $scope.googlePricelist = [];
  $scope.getGooglePricelist = function () {
    $scope.googlePricelist = [];
    var getUrl = 'https://sheets.googleapis.com/v4/spreadsheets/' + pricelistId + '/values/' + priceSheetName + '?key=' + googleApiKey;
    $http.get(getUrl)
      .then(function(result){
        $scope.googlePricelist = result.data.values;
        makePricelistObjects();
      });
  }
  
  
  $scope.columnsList = {
    lotNumber: {
      googleColumn: 0,
      columnName: "Lot Number",
      runFormat: function(data) {
        return Number(data)
      }
    },
    stage: {
      googleColumn: 1,
      columnName: "Stage",
    },
    landSize: {
      googleColumn: 2,
      columnName: "Land Size",
      runFormat: function(data) {
        return Number(parseFloat(data.replace(/[^\d\.]/g,'')))
      }
    }, // m2
    frontage: {
      googleColumn: 3,
      columnName: "Frontage",
      runFormat: function(data) {
        return Number(data)
      }
    }, // m
    depth: {
      googleColumn: 4,
      columnName: "Depth",
      runFormat: function(data) {
        return Number(data)
      }
    },
    landPrice: {
      googleColumn: 6,
      columnName: "Land Price",
      runFormat: function(data) {
        return Number(parseFloat(data.replace(/[^\d\.]/g,'')))
      }
    },
    hold: {
      googleColumn: 9,
      columnName: "Hold",
      runFormat: function(data) {
        if (data === "HOLD") {
          return true;
        } else {
          return false;
        }
      }
    },
    sold: {
      googleColumn: 9,
      columnName: "Sold",
      runFormat: function(data) {
        if (data === "SOLD") {
          return true;
        } else {
          return false;
        }
      }
    }
  };
  
  $scope.Object = Object;
  $scope.googleImportObject = {};
  function makePricelistObjects() {
    $scope.googleImportObject = {};
    angular.forEach($scope.googlePricelist, function(googleRow, rowNumber) {
      var lotId = googleRow[$scope.columnsList.stage.googleColumn] + '_' + (10000 + +googleRow[$scope.columnsList.lotNumber.googleColumn]);
      $scope.googleImportObject[lotId] = {
        lotId: lotId
      };
      
      angular.forEach($scope.columnsList, function(columnData, columnKey) {
        if (columnData.runFormat) {
          $scope.googleImportObject[lotId][columnKey] = columnData.runFormat(googleRow[columnData.googleColumn]);
        } else {
          $scope.googleImportObject[lotId][columnKey] = googleRow[columnData.googleColumn];
        }
        
      });
      
      if (rowNumber == $scope.googlePricelist.length -1) {
        console.log("$scope.googleImportObject: ", $scope.googleImportObject);
        $scope.checkGoogleUpdates();
      }
    })
  }
  
  $scope.googleImportObjectUpdates = {};
  $scope.googleImportObjectNew = {};
  $scope.checkGoogleUpdates = function () {
    $scope.googleImportObjectUpdates = {};
    $scope.googleImportObjectNew = {};
    var i = 0;
    angular.forEach($scope.googleImportObject, function(googleLand, lotId) {
      if ($scope.landResultsObject[lotId]) {
        angular.forEach($scope.columnsList, function(columnData, columnKey) {
          if ($scope.landResultsObject[lotId][columnKey] !== googleLand[columnKey]) {
            $scope.googleImportObjectUpdates[lotId] = true;
          }
        })
      } else {
        $scope.googleImportObjectNew[lotId] = true;
      }
      
      i++;
      if (i == Object.keys($scope.googleImportObject).length) {
        console.log("$scope.googleImportObjectUpdates: ", $scope.googleImportObjectUpdates);
        console.log("$scope.googleImportObjectNew: ", $scope.googleImportObjectNew);
      }
    })
  };
  
  $scope.showUpdatesOnly = false;
  $scope.hideNoUpdates = function() {
    $scope.showUpdatesOnly = !$scope.showUpdatesOnly;
  }
  
  /**
   * END - IMPORT LAND INFORMATION FROM GOOGLE SPREADSHEET
   */
  
  
  
  
  /**
   * UPDATE LAND DATA FROM GOOGLE IMPORT
   */
  $scope.updateAllFromGoogle = function() {
    var  updateThese = Object.keys($scope.googleImportObjectNew);
    angular.extend(updateThese, $scope.googleImportObjectUpdates);
    var noRefresh = true;
    
    angular.forEach(updateThese, function(lotId, key) {
      if (key == updateThese.length -1) {
        noRefresh = false;
      }
      
      if ($scope.googleImportObjectNew[lotId]) {
        $scope.save($scope.googleImportObject[lotId], noRefresh);
      } else if ($scope.googleImportObjectUpdates[lotId]) {
        $scope.updateLand($scope.landResultsObject[lotId], $scope.googleImportObject[lotId], noRefresh)
      }
    })
  };
  
  $scope.updateLand = function (oldDetails, newDetails, noRefresh) {
    var saveDetails = oldDetails;
    var i = 0;
    angular.forEach($scope.columnsList, function(columnData, columnKey) {
      saveDetails[columnKey] = newDetails[columnKey];
      
      i++;
      if (i == Object.keys($scope.columnsList).length) {
        $scope.save(saveDetails, noRefresh);
      }
    })
  }
  
  
  $scope.save = function(data, noRefresh){
    $http.post('/api/land/create', data)
      .then(function(result){
        if (!noRefresh) {
          getLand();
        }
      }, function(err){
        $scope.authError = (err.message || "Sorry, an error occurred. Please try again.");
      });
  };
  
  /**
   * END - UPDATE LAND DATA FROM GOOGLE IMPORT
   */
  
  
  
  /**
   * SELECTION HANDLER
   */
  $scope.selectedRows = {};
  $scope.selectRow = function(thisRow) {
    if ($scope.selectedRows[thisRow._id]) {
      delete $scope.selectedRows[thisRow._id];
    } else {
      $scope.selectedRows[thisRow._id] = thisRow;
    }
    console.log('$scope.selectedRows: ', $scope.selectedRows);
  }
  
  $scope.selectAllRows = function() {
    if (Object.keys($scope.selectedRows).length == $scope.landResults.length) {
      $scope.selectedRows = {};
    } else {
      angular.forEach($scope.landResults, function(thisRow, key) {
        $scope.selectedRows[thisRow._id] = thisRow;
      })
    }
  }
  
  $scope.deleteRows = function() {
    $http.post("/api/land/removeMany", {_id: {"$in": Object.keys($scope.selectedRows)}})
      .then(function (result) {
        console.log("deleteRows: ", result);
        getLand()
      }, function (err) {
        console.log("error: ", err);
      })
  }
  
  $scope.remove = function(data){
    var r = confirm("*WARNING* Are you sure you want to delete this pacakge?");
    if (r == true) {
      $http.post("/api/land/remove", data)
        .then(function (result) {
          console.log("removed content package: ", result);
          getLand()
        }, function (err) {
          console.log("error: ", err);
        })
    }
  };
  
  /**
   * END - SELECTION HANDLER
   */
  
});
