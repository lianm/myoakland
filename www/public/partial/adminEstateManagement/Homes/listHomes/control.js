angular.module('admin').controller('adminListHomesCtrl', function($scope, $http, $timeout){
  
  $scope.homeResults = [];
  function getHomes() {
    $http.post('/api/homes/findallPopulate', {})
      .then(function(result){
        $scope.homeResults = result.data;
        console.log("$scope.homeResults: ", $scope.homeResults);
      });
  }
  getHomes();
  
  
  $scope.Object = Object;
  $scope.selectedRows = {};
  $scope.selectRow = function(thisRow) {
    if ($scope.selectedRows[thisRow._id]) {
      delete $scope.selectedRows[thisRow._id];
    } else {
      $scope.selectedRows[thisRow._id] = thisRow;
    }
  }
  
  $scope.selectAllRows = function() {
    if (Object.keys($scope.selectedRows).length == $scope.homeResults.length) {
      $scope.selectedRows = {};
    } else {
      angular.forEach($scope.homeResults, function(thisRow, key) {
        $scope.selectedRows[thisRow._id] = thisRow;
      })
    }
  }
  
  
  
  $scope.deleteRows = function() {
    $http.post("/api/homes/removeMany", {_id: {"$in": Object.keys($scope.selectedRows)}})
      .then(function (result) {
        console.log("deleteRows: ", result);
        getHomes()
      }, function (err) {
        console.log("error: ", err);
      })
  }
  
  $scope.remove = function(data){
    var r = confirm("*WARNING* Are you sure you want to delete this home?");
    if (r == true) {
      $http.post("/api/homes/remove", data)
        .then(function (result) {
          console.log("removed content package: ", result);
          getHomes()
        }, function (err) {
          console.log("error: ", err);
        })
    }
  };
});
