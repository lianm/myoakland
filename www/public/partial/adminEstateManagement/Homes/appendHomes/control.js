angular.module('admin').controller('adminAppendHomesCtrl', function($scope, $state, $stateParams, $http, $timeout){
  
  $scope.page = {};
  function getHomes() {
    $http.post("/api/homes/findall", {_id: $stateParams.homeId})
      .then(function(result){
        console.log("getHomes: ", result);
        $scope.page = result.data[0];
      });
  }
  if ($stateParams.homeId) {
    getHomes();
  }
  
  $scope.buildersResults = [];
  function getBuilders() {
    $http.get('/api/builders/findall')
      .then(function(result){
        $scope.buildersResults = result.data;
        console.log("$scope.buildersResults: ", $scope.buildersResults);
      });
  }
  getBuilders();
  
  $scope.save = function(data){
    $http.post('/api/homes/create', data)
      .then(function(result){
        console.log("saved: ", result);
        if ($stateParams.homeId) {
          $scope.page = result.data;
        } else {
          $state.go('app.adminHomeId', {homeId: result.data._id})
        }
      }, function(err){
        $scope.authError = (err.message || "Sorry, an error occurred. Please try again.");
      });
  };
  
  $scope.remove = function(data){
    var r = confirm("*WARNING* Are you sure you want to delete this home?");
    if (r == true) {
      $http.post("/api/homes/remove", data)
        .then(function (result) {
          console.log("removed content home: ", result);
          $state.go('app.adminHomes')
        }, function (err) {
          console.log("error: ", err);
        })
    }
  };
  
});
