angular.module('admin').controller('appendPagesCtrl', function($scope, $rootScope, $state, $stateParams, $http, $timeout){
  
  $scope.templateFileNames = [
    {value: 'index'},
    {value: 'builder'},
    {value: 'builderfiles'},
    {value: 'home-land'},
    {value: 'homepage'},
    {value: 'index'},
    {value: 'land'},
    {value: 'landDetails'},
    {value: 'location'},
    {value: 'locationMap'},
    {value: 'masterplan'},
    {value: 'news'},
    {value: 'stageplans'}
  ];
  
  
  $rootScope.$watch('refreshDirective', function(newVal, oldVal) {
    if (oldVal !== newVal) {
      getPage();
    }
  })
  
  $scope.page = {};
  console.log("$scope.page: ", $scope.page);
  function getPage() {
    $http.post('/api/pages/findPage', {_id: $stateParams.pageId})
      .then(function(result){
        console.log("Loaded page: ", result);
        $scope.page = result.data[0];
      });
  }
  if ($stateParams.pageId) {
    getPage();
  }
  
  
  $scope.save = function(data){
    $scope.displaySaving = true;
    if ($scope.page.headerSlider === 'undefined') {
      $scope.page.headerSlider = null;
    }
    $http.post("/api/pages/create", data)
      .then(function(result){
        console.log("saved page: ", result);
        $scope.displaySaving = false;
        $scope.displaySaved = true;
        $timeout(function () {
          $scope.displaySaved = false;
        }, 6000);
        
        if ($stateParams.pageId) {
          $scope.page = result.data;
        } else {
          $state.go('app.adminPageId', {pageId: result.data._id})
        }
      }, function(err){
        $scope.authError = (err.message || "Sorry, an error occurred. Please try again.");
      });
  };
  
  
  $scope.remove = function(data){
    var r = confirm("Are you sure you want to delete this item?");
    if (r == true) {
      $http.post("/api/pages/remove", data)
        .then(function (result) {
          console.log("removed page: ", result);
          $state.go('app.adminPages')
        }, function (err) {
          $scope.authError = (err.message || "Sorry, an error occurred. Please try again.");
        });
    }
  };
  
  
  $scope.testsomething = {
    someSelectedContentBlock: ''
  }
  
  $scope.addContentType = function() {
    var addThis = {
      blockType: $scope.testsomething.someSelectedContentBlock
    };
    console.log("add content type: ", $scope.testsomething.someSelectedContentBlock);
    console.log("addThis: ", addThis);
    if (!$scope.page.contentBlocks) {
      $scope.page.contentBlocks = [];
    }
    $scope.page.contentBlocks.push(addThis)
  }
  
  
  
  $scope.contentBlockTypes = [];
  $scope.contentBlockTypesObject = {};
  function getContentBlockTypes() {
    $http.get("/api/contentblockTypes/findall")
      .then(function(result){
        console.log("Loaded getContentBlockTypes: ", result);
        $scope.contentBlockTypes = result.data;
        
        for (var i = 0; i < $scope.contentBlockTypes.length; i++) {
          $scope.contentBlockTypesObject[$scope.contentBlockTypes[i]._id] = $scope.contentBlockTypes[i];
        }
      });
  }
  getContentBlockTypes();
  
  
  $scope.blockShiftUp = function(thisContentBlockType) {
    var fromHere = $scope.page.contentBlocks.indexOf(thisContentBlockType);
    $scope.page.contentBlocks.splice(fromHere, 1);
    $scope.page.contentBlocks.splice(fromHere - 1, 0, thisContentBlockType);
  };
  $scope.blockShiftDown = function(thisContentBlockType) {
    var fromHere = $scope.page.contentBlocks.indexOf(thisContentBlockType);
    $scope.page.contentBlocks.splice(fromHere, 1);
    $scope.page.contentBlocks.splice(fromHere + 1, 0, thisContentBlockType);
  };
  
  $scope.blockDelete = function(thisContentBlockType) {
    var fromHere = $scope.page.contentBlocks.indexOf(thisContentBlockType);
    $scope.page.contentBlocks.splice(fromHere, 1);
  }
  
  
});
