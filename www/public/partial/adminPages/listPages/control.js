angular.module('admin').controller('listPagesCtrl', function($scope, $stateParams, $http, $timeout){
  
  $scope.pageResults = [];
  $scope.pageResultsObject = {};
  function getPages() {
    $http.get('/api/pages/findall')
      .then(function(result){
        console.log("Loaded pages: ", result);
        $scope.pageResults = result.data;
      });
  }
  getPages();
});
