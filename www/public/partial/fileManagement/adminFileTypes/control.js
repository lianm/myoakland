angular.module('admin').controller('adminFileTypesCtrl', function($scope, $http, $timeout){
  
  $scope.newFileType = {};
  $scope.saveFileType = function(thisFileType) {
    $http.post('/api/fileTypes/create', thisFileType)
      .then(function(result){
        console.log("add file types:", result);
        getFileTypes();
        $scope.newFileType = {};
      }, function(err){
        $scope.authError = (err.message || "Sorry, an error occurred. Please try again.");
      });
  }
  
  $scope.fileTypes = [];
  function getFileTypes() {
    $http.get('/api/fileTypes/findall')
      .then(function(result){
        $scope.fileTypes = result.data;
      });
  }
  getFileTypes();
  
  $scope.deleteFileType = function(deleteThis) {
    $http.post('/api/fileTypes/remove', deleteThis)
      .then(function(result){
        console.log("delete file types:", result);
        getFileTypes();
      }, function(err){
        $scope.authError = (err.message || "Sorry, an error occurred. Please try again.");
      });
  }
  
  
  $scope.page = {};
  $scope.hideThis = true;
  $scope.editPage = function(editthis) {
    $scope.page = editthis;
    $scope.hideThis = false;
  };
  
  $scope.hidePage = function() {
    $scope.hideThis = true;
    $scope.page = {};
  }
  
});
