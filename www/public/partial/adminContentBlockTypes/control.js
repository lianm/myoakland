angular.module('admin').controller('adminContentBlockTypesCtrl', function($scope, $http, $timeout){
  
  
  var pagetypeplural = "Pages";
  var pagetypesingle = "Page";
  var apiload = "/api/contentblockTypes/findall";
  var apiremove = "/api/contentblockTypes/remove";
  var apicreate = "/api/contentblockTypes/create";
  
  $scope.pagetypeplural = pagetypeplural;
  $scope.pagetypesingle = pagetypesingle;
  
  $scope.getTitle = function() {
    return pagetypeplural;
  };
  $scope.getSubtitle = function() {
    return "The " + pagetypeplural + " are listed below";
  };
  
  
  function resetPage() {
    $scope.page = {};
    $scope.uploadProgress_sliderImages = {};
    $scope.uploadProgress_sliderImages = {};
    setupPage();
  }
  resetPage();
  
  function setupPage() {
    if (!$scope.page.sliderImages) {
      $scope.page.sliderImages = [];
    };
  }
  
  
  $scope.hideThis = true;
  $scope.toggleAdd = function() {
    $scope.hideThis = false;
    resetPage();
  };
  
  
  $http.get(apiload)
    .then(function(result){
      console.log("Loaded " + pagetypeplural, result);
      $scope.results = result.data;
    });
  
  $scope.save = function(data){
    $http.post(apicreate, data)
      .then(function (result) {
        $http.get(apiload)
          .then(function (result) {
            console.log("Loaded " + pagetypeplural, result);
            $scope.results = result.data;
            resetPage();
          });
        $scope.hideThis = false;
      }, function (err) {
        console.log("error: ", err);
      })
  };
  
  $scope.editPage = function(editthis) {
    $scope.page = editthis;
    $scope.hideThis = false;
    setupPage();
  };
  
  
  
  /**
   * MEDIA DETAILS - PROJECT IMAGES & BROCHURE
   */
  
  $scope.$watch('sliderImages', function () {
    $scope.upload($scope.sliderImages, "sliderImages");
  });
  $scope.$watch('file', function () {
    if ($scope.file != null) {
      $scope.files = [$scope.file];
    }
  });
  
  $scope.upload = function (files, uploadType) {
    console.log("files: ", files);
    if (files && files.length) {
      for (var i = 0; i < files.length; i++) {
        var file = files[i];
        if (!file.$error) {
          cloudinary.upload(file, { /* cloudinary options here */ })
          // This returns a promise that can be used for result handling
            .then(function (resp) {
              console.log("image upload: ", resp);
              var saveThis = {
                originalName: resp.data.original_filename + "." + resp.data.format,
                storeName: resp.data.public_id
              };
              $scope.page[uploadType].push(saveThis);
              $timeout(function () {
                angular.forEach($scope[uploadType], function(uploadImage, thisIndex){
                  if (uploadImage.name === saveThis.originalName) {
                    $scope[uploadType].splice(thisIndex, 1);
                  }
                });
              }, 3000)
              
            }, function (resp) {
              console.log('Error status: ' + resp.status);
            }, function (evt) {
              var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
              var logName = "uploadProgress_" + uploadType;
              $scope[logName][evt.config._file.name] = progressPercentage;
            });
        }
      }
    }
  };
  
  $scope.imageShiftUp = function(thisImage) {
    var fromHere = $scope.page.sliderImages.indexOf(thisImage);
    $scope.page.sliderImages.splice(fromHere, 1);
    $scope.page.sliderImages.splice(fromHere - 1, 0, thisImage);
  };
  $scope.imageShiftDown = function(thisImage) {
    var fromHere = $scope.project.sliderImages.indexOf(thisImage);
    $scope.page.sliderImages.splice(fromHere, 1);
    $scope.page.sliderImages.splice(fromHere + 1, 0, thisImage);
  };
  $scope.imageDelete = function(thisImage) {
    var fromHere = $scope.page.sliderImages.indexOf(thisImage);
    $scope.page.sliderImages.splice(fromHere, 1);
  };
  
  
});
