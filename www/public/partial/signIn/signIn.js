angular.module('admin').controller('SigninCtrl',function($scope, $rootScope, app, $http, $state, authentication){
  
  $scope.app = app;
  
  $scope.login = function(username, password){
    console.log("Logging in user", username, password);
    $http.post("/api/user/login",{
      email:  username,
      password: password
    })
      .then(function (result) {
        console.log("login result", result);
        authentication.currentUser= result.data;
        $rootScope.currentUser = result.data;
        $state.go('app.home');
      }, function(err){
        $scope.authError = err.message || "An unknown error occurred, please try again.";
      })
  };
  
  $scope.signup = function(data) {
    console.log("Registering user", data);
    $http.post("/api/user/signup", data)
      .then(function (result) {
        $http.post("/api/user/login", {email: result.data.email, password: result.data.password})
          .then(function (result) {
            authentication.currentUser = result.data;
            $state.go('app.products');
          }, function (err) {
            $scope.authError = err.message || "An unknown error occurred, please try again.";
          })
      }, function (err) {
        $scope.authError = err.message || "An unknown error occurred, please try again.";
      })
  }
});
