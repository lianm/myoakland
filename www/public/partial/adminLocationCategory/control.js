angular.module('admin').controller('adminLocationCategoryCtrl', function($scope, $http, $timeout) {
  
  $scope.locationCategorys = [];
  function getLocationCategorys() {
    $http.get('/api/LocationCategory/findall')
      .then(function (result) {
        $scope.locationCategorys = result.data;
        console.log("$scope.locationCategorys: ", $scope.locationCategorys);
      }, function (err) {
        console.log("error: ", err);
      })
  }
  getLocationCategorys();
  
  $scope.save = function(data){
    $http.post('/api/LocationCategory/create', data)
      .then(function (result) {
        getLocationCategorys();
        $scope.hidePage();
      }, function (err) {
        console.log("error: ", err);
      })
  };
  
  $scope.hideThis = true;
  $scope.editPage = function(editthis) {
    $scope.page = editthis;
    $scope.hideThis = false;
  };
  
  $scope.hidePage = function() {
    $scope.hideThis = true;
    $scope.page = {};
  }
});
