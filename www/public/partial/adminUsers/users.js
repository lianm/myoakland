angular.module('admin').controller('userCtrl',function($scope, authentication, $http, $stateParams, $state){
  
  $scope.results = [];
  function getUsers() {
    $http.get("/api/user/findall")
      .then(function(result){
        console.log("$scope.results: ", $scope.results);
        $scope.results = result.data;
      })
  }
  getUsers();
  
  $scope.user = {};
  $scope.signup = function(data){
    $http.post("/api/user/create", data)
      .then(function(result){
        getUsers();
        $scope.showAdd = false;
      }, function(err){
        $scope.authError = (err.message || "Sorry, an error occurred. Please try again.");
      })
    $scope.user = {};
  }
  
  $scope.showAdd = false;
  $scope.edit = function(editthis) {
    $scope.user = editthis;
    $scope.showAdd = true;
  };
  
  $scope.toggleAdd = function() {
    $scope.showAdd = true;
    $scope.user = {};
  };
});
