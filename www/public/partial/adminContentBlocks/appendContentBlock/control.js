angular.module('admin').controller('adminAppendContentBlocksCtrl', function($scope, $rootScope, $state, $stateParams, $http, $timeout){
  
  $scope.contentBlockTypes = [];
  $scope.contentBlockTypesObject = {};
  function getContentBlockTypes() {
    $http.get("/api/contentblockTypes/findall")
      .then(function (result) {
        console.log("Loaded getContentBlockTypes: ", result);
        $scope.contentBlockTypes = result.data;
        for (var i = 0; i < $scope.contentBlockTypes.length; i++) {
          $scope.contentBlockTypesObject[$scope.contentBlockTypes[i]._id] = $scope.contentBlockTypes[i];
        }
      }, function (err) {
        console.log("error: ", err);
      })
  }
  getContentBlockTypes();
  
  $rootScope.$watch('refreshDirective', function(newVal, oldVal) {
    if (oldVal !== newVal) {
      getContentBlock();
    }
  });
  
  $scope.page = {};
  function getContentBlock() {
    $http.post("/api/contentblocks/findPage", {_id: $stateParams.blockId})
      .then(function(result){
        console.log("getContentBlock: ", result);
        $scope.page = result.data[0];
      });
  }
  if ($stateParams.blockId) {
    getContentBlock();
  }
  
  $scope.save = function(data){
    $scope.displaySaving = true;
    $http.post("/api/contentblocks/create", data)
      .then(function (result) {
        console.log("$scope.save: ", result);
        $scope.displaySaving = false;
        $scope.displaySaved = true;
        $timeout(function () {
          $scope.displaySaved = false;
        }, 6000);
        
        if ($stateParams.blockId) {
          $scope.page = result.data;
        } else {
          $state.go('app.adminContentBlockId', {blockId: result.data._id})
        }
      }, function (err) {
        console.log("error: ", err);
      })
  };
  
  
  $scope.remove = function(data){
    var r = confirm("Are you sure you want to delete this item?");
    if (r == true) {
      $http.post("/api/contentblocks/remove", data)
        .then(function (result) {
          console.log("removed content block: ", result);
          $state.go('app.adminContentBlocks')
        }, function (err) {
          console.log("error: ", err);
        })
    }
  };
  
  
});
