angular.module('admin').controller('adminListContentBlocksCtrl', function($scope, $http, $timeout){
  
  $scope.contentBlockTypes = [];
  $scope.contentBlockTypesObject = {};
  function getContentBlockTypes() {
    $http.get("/api/contentblockTypes/findall")
      .then(function (result) {
        console.log("Loaded getContentBlockTypes: ", result);
        $scope.contentBlockTypes = result.data;
        for (var i = 0; i < $scope.contentBlockTypes.length; i++) {
          $scope.contentBlockTypesObject[$scope.contentBlockTypes[i]._id] = $scope.contentBlockTypes[i];
        }
      }, function (err) {
        console.log("error: ", err);
      })
  }
  getContentBlockTypes();
  
  $scope.resultsContentBlocks = [];
  function getContentBlocks() {
    $http.get("/api/contentblocks/findall")
      .then(function(result){
        console.log("Loaded ContentBlocks: ", result);
        $scope.resultsContentBlocks = result.data;
        getContentBlockImages()
      });
  }
  getContentBlocks();
  
  
});
