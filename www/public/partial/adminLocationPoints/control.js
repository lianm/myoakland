angular.module('admin').controller('adminLocationPointsCtrl', function($scope, $http, $timeout) {
  
  $scope.locationPoints = [];
  function getLocationPoints() {
    $http.get('/api/LocationPoints/findall')
      .then(function (result) {
        $scope.locationPoints = result.data;
        console.log("$scope.locationPoints: ", $scope.locationPoints);
      }, function (err) {
        console.log("error: ", err);
      })
  }
  getLocationPoints();
  
  $scope.locationCategorys = [];
  function getLocationCategorys() {
    $http.get('/api/LocationCategory/findall')
      .then(function (result) {
        $scope.locationCategorys = result.data;
        console.log("$scope.locationCategorys: ", $scope.locationCategorys);
      }, function (err) {
        console.log("error: ", err);
      })
  }
  getLocationCategorys();
  
  $scope.save = function(data){
    $http.post('/api/LocationPoints/create', data)
      .then(function (result) {
        getLocationPoints();
        $scope.hidePage();
      }, function (err) {
        console.log("error: ", err);
      })
  };
  
  $scope.hideThis = true;
  $scope.editPage = function(editthis) {
    $scope.page = editthis;
    $scope.hideThis = false;
  };
  
  $scope.hidePage = function() {
    $scope.hideThis = true;
    $scope.page = {};
  }
});
