angular.module('admin').controller('adminMenuCtrl', function($scope, $http, $timeout) {
  
  $scope.menuResults = [];
  function getMenus() {
    $http.get('/api/menus/findall')
      .then(function (result) {
        $scope.menuResults = result.data;
        console.log("$scope.menuResults: ", $scope.menuResults);
      }, function (err) {
        console.log("error: ", err);
      })
  }
  getMenus();
  
  $scope.save = function(data){
    $http.post('/api/menus/create', data)
      .then(function (result) {
        getMenus();
        $scope.hidePage();
      }, function (err) {
        console.log("error: ", err);
      })
  };
  
  $scope.page = {};
  $scope.hideThis = true;
  $scope.editPage = function(editthis) {
    $scope.page = editthis;
    $scope.hideThis = false;
  };
  
  $scope.hidePage = function() {
    $scope.hideThis = true;
    $scope.page = {};
  }
  
  $scope.addParentMenu = {};
  $scope.addSubMenu = {};
  $scope.parentItemAdd = function(addParentMenu) {
    if (!$scope.page.parentMenus) {
      $scope.page.parentMenus = [];
    }
    $scope.page.parentMenus.push(addParentMenu);
    $scope.addParentMenu = {};
  }
  
  $scope.subItemAdd = function(thisSubMenu, thisParent) {
    var selectedParentMenu = $scope.page.parentMenus.indexOf(thisParent);
    if (!$scope.page.parentMenus[selectedParentMenu].subMenus) {
      $scope.page.parentMenus[selectedParentMenu].subMenus = [];
    }
    $scope.page.parentMenus[selectedParentMenu].subMenus.push(thisSubMenu);
    $scope.addSubMenu = {};
  }
  
  $scope.subItemDelete = function(thisSubMenu, thisParentMenu) {
    var selectedParentMenu = $scope.page.parentMenus.indexOf(thisParentMenu);
    var selectedSubMenu = $scope.page.parentMenus[selectedParentMenu].subMenus.indexOf(thisSubMenu);
    $scope.page.parentMenus[selectedParentMenu].subMenus.splice(selectedSubMenu, 1);
  }
  
  $scope.subItemUp = function(thisSubMenu, thisParentMenu) {
    var selectedParentMenu = $scope.page.parentMenus.indexOf(thisParentMenu);
    var selectedSubMenu = $scope.page.parentMenus[selectedParentMenu].subMenus.indexOf(thisSubMenu);
    $scope.page.parentMenus[selectedParentMenu].subMenus.splice(selectedSubMenu, 1);
    $scope.page.parentMenus[selectedParentMenu].subMenus.splice(selectedSubMenu - 1, 0, thisSubMenu);
  }
  
  $scope.subItemDown = function(thisSubMenu, thisParentMenu) {
    var selectedParentMenu = $scope.page.parentMenus.indexOf(thisParentMenu);
    var selectedSubMenu = $scope.page.parentMenus[selectedParentMenu].subMenus.indexOf(thisSubMenu);
    $scope.page.parentMenus[selectedParentMenu].subMenus.splice(selectedSubMenu, 1);
    $scope.page.parentMenus[selectedParentMenu].subMenus.splice(selectedSubMenu + 1, 0, thisSubMenu);
  }
  
  $scope.parentItemDelete = function(thisParentMenu) {
    var selectedParentMenu = $scope.page.parentMenus.indexOf(thisParentMenu);
    if ($scope.page.parentMenus[selectedParentMenu].subMenus) {
      if ($scope.page.parentMenus[selectedParentMenu].subMenus.length === 0) {
        $scope.page.parentMenus.splice(selectedParentMenu, 1);
      }
    } else {
      $scope.page.parentMenus.splice(selectedParentMenu, 1);
    }
  }
  
  $scope.parentItemUp = function(thisParentMenu) {
    var selectedParentMenu = $scope.page.parentMenus.indexOf(thisParentMenu);
    $scope.page.parentMenus.splice(selectedParentMenu, 1);
    $scope.page.parentMenus.splice(selectedParentMenu - 1, 0, thisParentMenu);
  }
  
  $scope.parentItemDown = function(thisParentMenu) {
    var selectedParentMenu = $scope.page.parentMenus.indexOf(thisParentMenu);
    $scope.page.parentMenus.splice(selectedParentMenu, 1);
    $scope.page.parentMenus.splice(selectedParentMenu + 1, 0, thisParentMenu);
  }
  
});
