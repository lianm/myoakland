angular.module('admin').controller('homepageCtrl', function($scope, $http, $timeout){
  
  
  var pagetypeplural = "Homepage Sections";
  var pagetypesingle = "Homepage Section";
  var apiload = "/api/homepage/findall";
  var apiremove = "/api/homepage/remove";
  var apicreate = "/api/homepage/create";
  
  $scope.pagetypeplural = pagetypeplural;
  $scope.pagetypesingle = pagetypesingle;
  
  $scope.getTitle = function() {
    return pagetypeplural;
  };
  $scope.getSubtitle = function() {
    return "The " + pagetypeplural + " are listed below";
  };
  
  $scope.page = {};
  function setupHomepageEdit() {
    $http.get(apiload)
      .then(function(result){
        if (result.length > 0) {
          $scope.page = result.data[0];
        }
        console.log("page", $scope.page);
        setupWhy();
      });
  }
  setupHomepageEdit();
  
  
  function setupWhy() {
    if (!$scope.page.why) {
      $scope.page.why = {};
      $scope.page.why.contentAreas = [];
      for (i = 0; i < 6; i++) {
        $scope.page.why.contentAreas[i] = {};
        $scope.page.why.contentAreas[i].title = 'Enter Title ' + i;
        $scope.page.why.contentAreas[i].content = 'Enter Content';
      }
    }
  }
  
  $scope.editWhyIndex = 0;
  $scope.editWhyContent = function(why) {
    $scope.editWhyIndex = $scope.page.why.contentAreas.indexOf(why);
  };
  
  $scope.setWhyStyle = function(why) {
    var use = {
      "background-color": why.backgroundColor,
      "color": why.textColor
    };
    return use;
  };
  
  
  
  
  
  
  $scope.save = function(data){
    $http.post(apicreate, data)
      .then(function(result){
        setupHomepageEdit();
      }, function(err){
        $scope.authError = (err.message || "Sorry, an error occurred. Please try again.");
      });
  };
  
  
  
  
  /**
   * MEDIA DETAILS - PROJECT IMAGES & BROCHURE
   */
  
  $scope.uploadProgress_sliderImages = {};
  
  function setupPage() {
    if (!$scope.page.sliderImages) {
      $scope.page.sliderImages = [];
    };
  }
  setupPage();
  
  $scope.$watch('sliderImages', function () {
    $scope.upload($scope.sliderImages, "sliderImages");
  });
  $scope.$watch('file', function () {
    if ($scope.file != null) {
      $scope.files = [$scope.file];
    }
  });
  
  $scope.upload = function (files, uploadType) {
    console.log("files: ", files);
    if (files && files.length) {
      for (var i = 0; i < files.length; i++) {
        var file = files[i];
        if (!file.$error) {
          cloudinary.upload(file, { /* cloudinary options here */ })
          // This returns a promise that can be used for result handling
            .then(function (resp) {
              console.log("image upload: ", resp);
              var saveThis = {
                originalName: resp.data.original_filename + "." + resp.data.format,
                storeName: resp.data.public_id
              };
              $scope.page[uploadType].push(saveThis);
              $timeout(function () {
                angular.forEach($scope[uploadType], function(uploadImage, thisIndex){
                  if (uploadImage.name === saveThis.originalName) {
                    $scope[uploadType].splice(thisIndex, 1);
                  }
                });
              }, 3000)
              
            }, function (resp) {
              console.log('Error status: ' + resp.status);
            }, function (evt) {
              var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
              var logName = "uploadProgress_" + uploadType;
              $scope[logName][evt.config._file.name] = progressPercentage;
            });
        }
      }
    }
  };
  
  $scope.imageShiftUp = function(thisImage) {
    var fromHere = $scope.page.sliderImages.indexOf(thisImage);
    $scope.page.sliderImages.splice(fromHere, 1);
    $scope.page.sliderImages.splice(fromHere - 1, 0, thisImage);
  };
  $scope.imageShiftDown = function(thisImage) {
    var fromHere = $scope.project.sliderImages.indexOf(thisImage);
    $scope.page.sliderImages.splice(fromHere, 1);
    $scope.page.sliderImages.splice(fromHere + 1, 0, thisImage);
  };
  $scope.imageDelete = function(thisImage) {
    var fromHere = $scope.page.sliderImages.indexOf(thisImage);
    $scope.page.sliderImages.splice(fromHere, 1);
  };
  
  
  
  
  
  /**
   * ASSOCIATE LINKS
   */
  
  
  $scope.uploadProgress_associateLogos = {};
  
  function setupAssociateLogos() {
    if (!$scope.page.associateLinks) {
      $scope.page.associateLinks = {};
    };
    if (!$scope.page.associateLinks.logoLinks) {
      $scope.page.associateLinks.logoLinks = [];
    };
  }
  setupAssociateLogos();
  
  
  $scope.$watch('associateLogos', function () {
    $scope.uploadAssociateLogos($scope.associateLogos);
  });
  $scope.$watch('associateLogosFile', function () {
    if ($scope.associateLogosFile != null) {
      $scope.associateLogosFiles = [$scope.associateLogosFile];
    }
  });
  
  $scope.uploadAssociateLogos = function (associateLogosFiles) {
    if (associateLogosFiles && associateLogosFiles.length) {
      for (var i = 0; i < associateLogosFiles.length; i++) {
        var associateLogosFile = associateLogosFiles[i];
        if (!associateLogosFile.$error) {
          cloudinary.upload(associateLogosFile, { /* cloudinary options here */ })
          // This returns a promise that can be used for result handling
            .then(function (resp) {
              console.log("image upload: ", resp);
              var saveThis = {
                originalName: resp.data.original_filename + "." + resp.data.format,
                storeName: resp.data.public_id
              };
              $scope.page.associateLinks.logoLinks.push(saveThis);
              $timeout(function () {
                angular.forEach($scope.associateLogos, function(uploadImage, thisIndex){
                  if (uploadImage.name === saveThis.originalName) {
                    $scope.associateLogos.splice(thisIndex, 1);
                  }
                });
              }, 3000)
              
            }, function (resp) {
              console.log('Error status: ' + resp.status);
            }, function (evt) {
              var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
              $scope.uploadProgress_associateLogos[evt.config._file.name] = progressPercentage;
            });
        }
      }
    }
  };
  
  $scope.associateLogoShiftUp = function(thisImage) {
    var fromHere = $scope.page.associateLinks.logoLinks.indexOf(thisImage);
    $scope.page.associateLinks.logoLinks.splice(fromHere, 1);
    $scope.page.associateLinks.logoLinks.splice(fromHere - 1, 0, thisImage);
  };
  $scope.associateLogoShiftDown = function(thisImage) {
    var fromHere = $scope.page.associateLinks.logoLinks.indexOf(thisImage);
    $scope.page.associateLinks.logoLinks.splice(fromHere, 1);
    $scope.page.associateLinks.logoLinks.splice(fromHere + 1, 0, thisImage);
  };
  $scope.associateLogoDelete = function(thisImage) {
    var fromHere = $scope.page.associateLinks.logoLinks.indexOf(thisImage);
    $scope.page.associateLinks.logoLinks.splice(fromHere, 1);
  };
  
  
});
