angular.module('admin').controller('AppCtrl', function ($scope, $localStorage, $window, app, authentication, $state) {
  
  
  //$scope.$watch('authentication.currentUser', function(){
  //    if(!authentication.currentUser){
  //        console.log("Redirecting to sign in");
  //        $window.location.replace("/#/signIn");
  //    } else {
  //        console.log("Did not redirect, because user is currently logged in");
  //    }
  //});
  
  
  function isSmartDevice() {
    // Adapted from http://www.detectmobilebrowsers.com
    var ua = $window['navigator']['userAgent'] || $window['navigator']['vendor'] || $window['opera'];
    // Checks for iOs, Android, Blackberry, Opera Mini, and Windows mobile devices
    return (/iPhone|iPod|iPad|Silk|Android|BlackBerry|Opera Mini|IEMobile/).test(ua);
  }
  
  // add 'ie' classes to html
  var isIE = !!navigator.userAgent.match(/MSIE/i);
  if (isIE) {
    angular.element($window.document.body).addClass('ie');
  }
  if (isSmartDevice()) {
    angular.element($window.document.body).addClass('smart');
  }
  
  $scope.app = app;
  
  // angular translate
  $scope.lang = {isopen: false};
  $scope.langs = {en: 'English', de_DE: 'German', it_IT: 'Italian'};
  //$scope.selectLang = $scope.langs[$translate.proposedLanguage()] || "English";
  //$scope.setLang = function (langKey, $event) {
  //    // set the current lang
  //    $scope.selectLang = $scope.langs[langKey];
  //    // You can change the language during runtime
  //    $translate.use(langKey);
  //    $scope.lang.isopen = !$scope.lang.isopen;
  //};
  
  $scope.$watch('app.settings', function () {
    if ($scope.app.settings.asideDock && $scope.app.settings.asideFixed) {
      // aside dock and fixed must set the header fixed.
      $scope.app.settings.headerFixed = true;
    }
    // save to local storage
    $localStorage.settings = $scope.app.settings;
  }, true);
  
  
});
