angular.module('admin').controller('appendContactFormCtrl',function($scope, authentication, $http, $stateParams, $state){
  
  $scope.enquiry = {};
  function getEnquiry() {
    $http.post('/api/emails/findPage', {_id: $stateParams.enquiryId})
      .then(function(result){
        console.log("Loaded page: ", result);
        $scope.enquiry = result.data[0];
      });
  }
  if ($stateParams.enquiryId) {
    getEnquiry();
  }
  
  $scope.save = function(data){
    $scope.displaySaving = true;
    $http.post("/api/emails/create", data)
      .then(function(result){
        console.log("saved page: ", result);
        $scope.displaySaving = false;
        $scope.displaySaved = true;
        $timeout(function () {
          $scope.displaySaved = false;
        }, 6000);
        
        if ($stateParams.enquiryId) {
          $scope.enquiry = result.data;
        } else {
          $state.go('app.adminContactFormId', {enquiryId: result.data._id})
        }
      }, function(err){
        $scope.authError = (err.message || "Sorry, an error occurred. Please try again.");
      });
  };
  
  
  $scope.remove = function(data){
    var r = confirm("Are you sure you want to delete this item?");
    if (r == true) {
      $http.post("/api/emails/remove", data)
        .then(function (result) {
          console.log("removed page: ", result);
          $state.go('app.adminContactForm')
        }, function (err) {
          $scope.authError = (err.message || "Sorry, an error occurred. Please try again.");
        });
    }
  };
  
});
