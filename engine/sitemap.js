var _ = require('lodash');
var url = require("url");
var Q = require('q');
var async = require('async');

var Pages = require('../api/routes/Pages');

/**
 * GENERATE SITEMAP
 */

var sm = require('sitemap');
var sitemap = sm.createSitemap ({
  hostname: 'http://somerfieldestate.com.au/',
  cacheTime: 600000,
  urls: [
    { url: '/' , changefreq: 'weekly', priority: 0.8, lastmodrealtime: true}
  ]
});
var lastCleanSitemap = "";

function cleanSitemap() {
  var deferred = Q.defer();
  
  if (((new Date) - lastCleanSitemap) > (60 * 60 * 1000)) {
    sitemap = sm.createSitemap ({
      hostname: 'http://somerfieldestate.com.au/',
      cacheTime: 600000,
      urls: [
        { url: '/' , changefreq: 'weekly', priority: 0.8, lastmodrealtime: true}
      ]
    });
    lastCleanSitemap = Date.now();
    
    Q.all([
      sitemapPages()
    ])
      .then(function(qResults) {
        var returnThis = qResults;
        returnThis["cleanSitemap updated"] = true;
        deferred.resolve(returnThis);
      })
  } else {
    deferred.resolve({"cleanSitemap updated": false});
  }
  
  return deferred.promise;
}
cleanSitemap();

function sitemapPages() {
  var deferred = Q.defer();
  
  Pages.searchPages({"cms.showInSitemap": true})
    .then(function(pageContent) {
      for(var i = 0; i < pageContent.length; i++) {
        sitemap.add({
          url: "i/" + pageContent[i].cms.pageUrl,
          lastmodISO: pageContent[i].cms.updated_at,
          changefreq: 'weekly',
          priority: 0.5,
          mobile: true
        });
        if (i === pageContent.length -1) {
          deferred.resolve({"sitemapPages": true});
        }
      }
    })
  
  return deferred.promise;
}


module.exports = function(app) {
  
  app.get('/sitemap.xml', function(req, res) { // send XML map
    cleanSitemap()
      .then(function(result) {
        console.log("sitemap.xml result: ", result);
        sitemap.toXML( function (err, xml) {
          if (err) {
            return res.status(500).end();
          }
          res.header('Content-Type', 'application/xml');
          res.send( xml );
        });
      })
    
  }).get('/robots.txt', function(req, res) { // send TXT map
  
  
  });
}
