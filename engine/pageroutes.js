/**
 * Setup the static views
 * ----------------------
 * Remove the default one below to just use static HTML
 * in the www/public directory
 */

var _ = require('lodash');
var url = require("url");
var Q = require('q');
var async = require('async');
var moment = require('moment');


var Packages = require('../api/routes/Packages');
var Land = require('../api/routes/Land');
var Pages = require('../api/routes/Pages');
var LocationPoints = require('../api/routes/LocationPoints');
var Builders = require('../api/routes/Builders');
var Menus = require('../api/routes/Menus');
var Facebook = require('./facebook');


var siteVariables = {
  siteName: 'Somerfield Estate, Holmview',
  email: 'sales@somerfieldestate.com.au',
  phone: '07 5609 8491',
  address1: '276 Wuraga Road',
  addressSuburb: 'Holmview, QLD 4207',
  facebook: 'somerfieldestate'
};

var searchRanges = {
  landPrice: {
    type: 'minMaxSelect',
    name: 'Land Price',
    items: [
      {text: 'ANY', value: null},
      {text: '$150,000', value: 150000},
      {text: '$175,000', value: 175000},
      {text: '$200,000', value: 200000},
      {text: '$225,000', value: 225000},
      {text: '$250,000', value: 250000},
      {text: '$300,000', value: 300000},
      {text: '$300,000+', value: 1000000}
    ]
  },
  landSize: {
    type: 'minMaxSelect',
    name: 'Land Size',
    items: [
      {text: 'ANY', value: null},
      {text: '250m2', value: 250},
      {text: '300m2', value: 300},
      {text: '350m2', value: 350},
      {text: '400m2', value: 400},
      {text: '450m2', value: 450},
      {text: '500m2', value: 500},
      {text: '550m2', value: 550},
      {text: '600m2', value: 600},
      {text: '650m2', value: 650},
      {text: '700m2', value: 700},
      {text: '750m2', value: 750},
      {text: '800m2', value: 800},
      {text: '850m2', value: 850},
      {text: '900m2', value: 900},
      {text: '950m2', value: 950},
      {text: '1000m2', value: 1000},
      {text: '1000m2+', value: 10000000}
    ]
  },
  frontage: {
    type: 'minMaxSelect',
    name: 'Frontage',
    items: [
      {text: 'ANY', value: null},
      {text: '10m', value: 10},
      {text: '10.5m', value: 10.5},
      {text: '11m', value: 11},
      {text: '11.5m', value: 11.5},
      {text: '12m', value: 12},
      {text: '12.5m', value: 12.5},
      {text: '13m', value: 13},
      {text: '13.5m', value: 13.5},
      {text: '14m', value: 14},
      {text: '14.5m', value: 14.5},
      {text: '15m', value: 15},
      {text: '15.5m', value: 15.5},
      {text: '16m', value: 16},
      {text: '17m', value: 17},
      {text: '18m', value: 18},
      {text: '19m', value: 19},
      {text: '20m', value: 20},
      {text: '30m', value: 30},
      {text: '40m', value: 40},
      {text: '50m', value: 50}
    ]
  },
  bedrooms: {
    type: 'inlineSelect',
    name: 'Bedrooms',
    items: [
      {text: 'ANY', value: null},
      {text: '1', value: 1},
      {text: '2', value: 2},
      {text: '3', value: 3},
      {text: '4', value: 4},
      {text: '5', value: 5}
    ]
  },
  bathrooms: {
    type: 'inlineSelect',
    name: 'Bathrooms',
    items: [
      {text: 'ANY', value: null},
      {text: '1', value: 1},
      {text: '2', value: 2},
      {text: '3', value: 3}
    ]
  },
  cars: {
    type: 'inlineSelect',
    name: 'Cars',
    items: [
      {text: 'ANY', value: null},
      {text: '1', value: 1},
      {text: '2', value: 2}
    ]
  },
  builder: {
    type: 'dropdownSelect',
    name: 'Builder',
    items: [
      {text: 'ANY', value: null},
      {text: 'metricon', value: 'metricon'},
      {text: 'adenbrook', value: 'adenbrook'}
    ]
  },
  houseLevels: {
    type: 'inlineSelect',
    name: 'House Levels',
    items: [
      {text: 'ANY', value: null},
      {text: '1', value: 1},
      {text: '2', value: 2}
    ]
  },
  houseSize: {
    type: 'minMaxSelect',
    name: 'House Size',
    items: [
      {text: 'ANY', value: null},
      {text: '140m2 ~ 15.07sq', value: 140},
      {text: '160m2 ~ 17.22sq', value: 160},
      {text: '180m2 ~ 19.37sq', value: 180},
      {text: '200m2 ~ 21.53sq', value: 200},
      {text: '220m2 ~ 23.68sq', value: 220},
      {text: '240m2 ~ 25.83sq', value: 240},
      {text: '260m2 ~ 27.99sq', value: 260},
      {text: '280m2 ~ 30.14sq', value: 280},
      {text: '300m2 ~ 32.29sq', value: 300},
      {text: '350m2 ~ 37.67sq', value: 350}
    ]
  },
  packagePrice: {
    type: 'minMaxSelect',
    name: 'Package Price',
    items: [
      {text: 'ANY', value: null},
      {text: '$300,000', value: 300000},
      {text: '$350,000', value: 350000},
      {text: '$400,000', value: 400000},
      {text: '$450,000', value: 450000},
      {text: '$500,000', value: 500000},
      {text: '$550,000', value: 550000},
      {text: '$600,000', value: 600000},
      {text: '$650,000', value: 650000}
    ]
  }
};

var lastCacheCheck = '';
var cacheTime = 5*60*1000; // 5 minutes
function checkSearchRanges() {
  var deferred = Q.defer();
  
  if ((new Date() - lastCacheCheck) > cacheTime) {
    console.log("reload cached items");
    Builders.getBuildersSearchRange()
      .then(function(builders) {
        searchRanges.builder.items = builders;
        lastCacheCheck = new Date();
        deferred.resolve(searchRanges);
      })
  } else {
    deferred.resolve(searchRanges);
  }
  
  return deferred.promise;
};

function processSession(currentSession) {
  var deferred = Q.defer();
  
  deferred.resolve(currentSession);
  
  return deferred.promise;
};


function siteStructure(req, pageSearch) {
  var deferred = Q.defer();
  
  Q.all([
    Pages.pageSearch(pageSearch || {}),
    Menus.getMenus(),
    checkSearchRanges(),
    processSession(req.session),
    Land.landScroller({sold: {$ne: true}}),
    Packages.packageScroller()
  ])
    .then(function(qResults){
      var returnThis = {
        title: qResults[0].pageContent.name,
        pageContent: qResults[0].pageContent,
        blockContent: qResults[0].contentBlocks,
        pageHeaderSlider: qResults[0].pageHeaderSlider,
        menus: qResults[1],
        searchRanges: qResults[2],
        session: qResults[3],
        landScroller: qResults[4],
        packageScroller: qResults[5]
      };
      
      deferred.resolve(returnThis);
    });
  
  return deferred.promise;
}



module.exports = function(app) {
  app.get('/', function (req, res) {
    var pageUrl = 'homepage';
    
    siteStructure(req, {"cms.pageUrl": pageUrl})
      .then(function(pageContent) {
        if (pageContent.error === true) {
          res.render('error', {});
        } else {
          var returnThis = pageContent;
          returnThis.session = req.session;
          res.render('homepage', returnThis);
        }
      });
  });
  
  app.get('/i/faqs', function (req, res) {
    Q.all([
      siteStructure(req, {"cms.pageUrl": 'land'})
    ])
      .then(function(qResults){
        var returnThis = qResults[0];
      
        res.render('faq', returnThis);
      })
  })
  
  app.get('/i/plans', function (req, res) {
    Q.all([
      siteStructure(req, {"cms.pageUrl": 'land'})
    ])
      .then(function(qResults){
        var returnThis = qResults[0];
        
        res.render('plans', returnThis);
      })
  })
  
  app.get('/i/for-sale', function (req, res) {
    Q.all([
      siteStructure(req, {"cms.pageUrl": 'land'})
    ])
      .then(function(qResults){
        var returnThis = qResults[0];
        
        res.render('units', returnThis);
      })
  })
  app.get('/i/for-sale2', function (req, res) {
    Q.all([
      siteStructure(req, {"cms.pageUrl": 'land'})
    ])
      .then(function(qResults){
        var returnThis = qResults[0];
        
        res.render('units2', returnThis);
      })
  })
  
  /*
  app.get('/i/locationmap', function (req, res) {
    Q.all([
      Pages.pageSearch({'cms.pageUrl': 'locationmap'}),
      LocationPoints.pointSearch({})
    ])
      .then(function(qResults) {
        var pageContent = qResults[0].pageContent;
        var locationPoints = qResults[1];
        var mainMenu = qResults[0].mainMenu;
        
        console.log("got to here!", locationPoints.length);
        
        if (locationPoints.length > 0) {
          console.log("found locationPoints");
          
          var locationCategories = {};
          locationCategories.unknown = {
            name: 'unknown',
            _id: 'unknown'
          };
          var groupedLocations = {};
          
          for (var i = 0; i < locationPoints.length; i++) {
            var thisCategory = 'unknown';
            if (locationPoints[i].category) {
              thisCategory = locationPoints[i].category._id;
              locationCategories[thisCategory] = locationPoints[i].category;
            }
            console.log("thisCategory: ", thisCategory);
            
            if (!groupedLocations[thisCategory]) {
              groupedLocations[thisCategory] = [];
            }
            groupedLocations[thisCategory].push(locationPoints[i]);
            
            if (i === locationPoints.length -1) {
              res.render('locationmap', {
                title: pageContent.name,
                pageContent: pageContent,
                locationPoints: locationPoints,
                mainMenu: mainMenu,
                groupedLocations: groupedLocations,
                locationCategories: locationCategories
              });
            }
          }
        } else {
          console.log("no locationPoints");
          res.render('locationmap', {
            title: pageContent.name,
            pageContent: pageContent,
            locationPoints: locationPoints,
            mainMenu: mainMenu,
            groupedLocations: groupedLocations,
            locationCategories: locationCategories
          });
        }
      })
  });
  */
  
  app.get('/i/news-and-events', function (req, res) {
    Q.all([
      siteStructure(req, {"cms.pageUrl": 'news-and-events'}),
      Facebook.facebookFeed()
    ])
      .then(function(qResults){
        var returnThis = qResults[0];
        returnThis.facebookFeed = qResults[1];
        returnThis.dateFunction = function(itemDate) {
          return moment(itemDate).fromNow();
        }
        
        res.render('news', returnThis);
      })
  });
  
  app.get('/i/:pageUrl', function (req, res) {
    var pageUrl = 'homepage';
    if (req.params.pageUrl) {
      pageUrl = req.params.pageUrl;
    }
  
    siteStructure(req, {"cms.pageUrl": pageUrl})
      .then(function(pageResults){
        res.render((pageResults.pageContent.cms.pageTemplate || 'index'), pageResults);
      })
  });
  
  
  app.get(function (req, res) {
    res.status(404).render('error', {});
  });
}