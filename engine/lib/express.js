/**
 * This is just essentially the output you get when you generate an express app using the CLI tool
 *
 * It has been modified a little to work with the system - most notably breaking down the workflow into two functions
 * so other routes can be injected.
 */


// Express Dependencies
// --------------------
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var helmet = require('helmet');
var cors = require('cors');
var compression = require('compression');

// Initialise express
// ------------------
var app = express();

// Setup default middleware
// ------------------------
app.use(logger('dev'));



var sess = {
  secret: '1234890sdlkjfu02',
  resave: true,
  saveUninitialized: true,
  rolling: true,
  cookie: {},
  store: new MongoStore({url: process.env.DB_URI})
};

if(process.env.NODE_ENV == "production"){
  app.set('trust proxy', 1); // trust first proxy
  sess.cookie.secure = 'auto'; // serve secure cookies
} else {
  sess.cookie.maxAge = 6000000;
}

app.use(express.static(path.join(__dirname, "..", "..", "www", "public")));
app.use(session(sess));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(helmet());
app.use(cors());
app.use(compression());

/**
 * This function is called after the rest of the loader functions have been initialised
 * It is very important to ensure that all express routes have been loaded before calling this function
 * @param port
 * @param dontListen - Used for testing with supertest
 */
function listen(port, dontListen, production) {
  // Setup the view engine
  // ---------------------
  var viewPath = path.join(__dirname, "..", "..", "www", 'views');
  app.set('views', viewPath);
  app.set('view engine', 'pug');
  
  // Setup the path to the static files
  // ----------------------------------
  
  /*
  if(process.env.NODE_ENV == "production"){
    app.use(express.static(path.join(__dirname, "..", "..", "www", "public", "dist")));
  } else {
    app.use(express.static(path.join(__dirname, "..", "..", "www", "public")));
  }
  */
  
  
  app.use('/admin', express.static(path.join(__dirname, "..", "..", "www", "public")));
  
  
  
  // Fallover to a 404 if no other routes are matched
  // ------------------------------------------------
  app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });
  
  
  
  
  
  
  // Error Handlers
  // --------------
  
  //Development error handler.
  //Will print a stacktrace
  console.log("Current Environment: ", app.get('env'));
  app.use(function (err, req, res, next) {
    if (req.get('content-type') == "application/json") {
      var errOutput = {
        message: err.message,
        stack: err.stack
      }
      res.status(err.status).json(errOutput)
    } else {
      if(err.status){
        res.status(err.status);
      } else {
        res.status(500);
      }
      
      console.log(err);
      
      res.render('error', {
        message: err.message,
        error: err
      });
    }
  });
  
  var server = app.listen(process.env.PORT || port, function () {
    console.log('Listening on port %d', server.address().port);
  });
  
}

var expressApp = {
  app: app,
  listen: listen
};

module.exports = expressApp;
