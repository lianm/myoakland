var Q = require('q');
var async = require('async');
var router = require('express').Router();
var path = require('path');
var walk = require('walk');
var _ = require('lodash');

var apiFolder = './../../api/models/';
apiModels = {};

function loadModels() {
  var deferred = Q.defer();

  var walker = walk.walk('./api/models', {followLinks: false});
  walker.on('file', function (root, stat, next) {
    var current = path.join(root, stat.name);
    var extname = path.extname(current);

    if (extname === '.js') {
      var split = stat.name.split(".");
      if (split.length == 2) {
        var thisModel = stat.name.split(".")[0];
        apiModels[thisModel.toLowerCase()] = require(apiFolder + stat.name.split(".")[0]).model;
      }
    }
    next();
  });

  walker.on('end', function () {
    deferred.resolve('successful');
  });

  return deferred.promise;
}


function loadPages() {
  var deferred = Q.defer();

  loadModels()
    .then(function() {
      apiModels.pages.find({}, function(err, docs) {
        if (err) {
          console.log("no pages", err);
          return done(err);
        }
        if (docs) {
          async.each(docs, function(doc, nextDoc) {
            express.app.use('/'+ doc.cms.pageUrl, function(req, res) {
              console.log(req);
              var baseUrl = req.baseUrl;
              baseUrl = baseUrl.substring(1);
              apiModels.pages.findOne({'cms.pageUrl': baseUrl}, function(err, doc) {
                var errorReturn = {
                  status: 404,
                  url: req.url,
                  pageContent: {
                    pageTitle: 'Error 404'
                  }
                };
                if (err) {
                  console.log(err);
                  res.render('error', errorReturn);
                } else {
                  if (doc) {
                    var pageTemplate = 'index';
                    if (doc.cms.pageTemplate) {
                      pageTemplate = doc.cms.pageTemplate;
                    }
                    var pageData = {
                      pageContent: doc
                    };
                    if (doc.cms.includeModels) {
                      var searchModel = doc.cms.includeModels;
                      apiModels[searchModel.toLowerCase()].find({}, function(err, subDocs) {
                        if (err) {
                          console.log('sub document error');
                        }
                        if (subDocs) {
                          pageData.subPages = subDocs;
                        }
                        res.render(pageTemplate, pageData);
                      });
                    } else {
                      res.render(pageTemplate, pageData);
                    }
                  } else {
                    console.log(err);
                    res.render('error', errorReturn);
                  }

                }
              })
            });
            nextDoc();
          }, function(err) {
            if (err) {
              console.log("pages error: ", err);
              deferred.resolve('successful');
            } else {
              deferred.resolve('successful');
            }
          })
        }
      });
    });

  return deferred.promise;
}



module.exports = {
  loadPages: loadPages
};