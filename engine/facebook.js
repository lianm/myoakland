var Q = require('q');

var FB = require('fb');
// var facebook = new Facebook({ appID: '1975481136055984', secret: '82f4895872b8781da4a0aa49a4f9ab3d' });
FB.setAccessToken('1975481136055984|QKWjzUPw3LA6Fuvj22LLsyo7CVg');

var facebookPage = '784755994901933';

function facebookFeed() {
  var deferred = Q.defer();
  
  FB.api(facebookPage +
    '?fields=id,name,picture,' +
    'posts{' +
      'message,created_time,type,full_picture,link,child_attachments,shares,' +
      'comments.summary(true),' +
      'likes.summary(true)' +
    '}', function(res) {
      if (!res || res.error) {
        console.log('facebookFeed err: ', res.error);
        deferred.resolve({error: true, errorMessage: res.error});
      } else {
        deferred.resolve(res);
      }
  });
  
  return deferred.promise;
}


module.exports = {
  facebookFeed: facebookFeed
}
