// Load Dependencies
// -----------------
express = require('./lib/express');
var mongoose = require('mongoose');
mongoose.Promise = require('q').Promise;

var loader = require('./lib/loader');
var cms = require('./lib/cms');


/**
 * Initialise the engine.
 * ----------------------
 * Call this after setting the appropriate functions
 */

const dbOptions = {
  useMongoClient: true,
  autoIndex: false, // Don't build indexes
  reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
  reconnectInterval: 500, // Reconnect every 500ms
  poolSize: 10, // Maintain up to 10 socket connections
  // If not connected, return errors immediately rather than waiting for reconnect
  bufferMaxEntries: 0,
  connectTimeoutMS: 10000, // Give up initial connection after 10 seconds
  socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
};


function init() {
  mongoose.connect(engine.config.dbUri, dbOptions)
    .then(function(dbResults) {
      console.log("Mongo Connected: " + dbResults.host + ':' + dbResults.port);
      console.log("Mongo user / pass: " + dbResults.user + ' / ' + dbResults.pass);
      console.log("Mongo Database: " + dbResults.name);
    })
    .catch(function(err) {
      console.log("mongoose connection error: ", err);
    });
  
  loader.loadApi()
    .then(function (router) {
      express.app.use(engine.config.apiPrefix, router);
      express.listen(8190);
    });
}

process.on('warning', (warning) => {
  console.warn(warning.name + ': ' + warning.message);    // Print the warning name
console.warn(warning.stack);   // Print the stack trace
});


/**
 * The engine object.
 * ------------------
 * @type {{config: {port: (*|number), dbUri: *, apiPrefix: string}, version: string, express: (expressApp|exports|*), mongoose: (*|exports), init: init}}
 */
var engine = {
  config: {
    port: process.env.port || 8186,
    dbUri: process.env.DB_URI,
    apiPrefix: "/api"
  },
  version: "0.0.1",
  express: express,
  mongoose: mongoose,
  init: init
};

module.exports = engine;
